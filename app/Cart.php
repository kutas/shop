<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Cart extends Model
{

    protected $fillable = ['paid'];

    //Relations

    public function cartItems()
    {
        return $this->hasMany(CartItem::class);
    }

}
