<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CartItem extends Model
{
    protected $fillable = ['product_id', 'quantity'];

    //Relations
    public function modifiers()
    {
        return $this->hasMany(CartItemModifier::class);
    }

    public function inputs()
    {
        return $this->hasMany(CartItemInput::class);
    }

    public function product()
    {
        return $this->belongsTo(Product::class);
    }
}
