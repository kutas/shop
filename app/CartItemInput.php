<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CartItemInput extends Model
{
    protected $fillable = [
        'modifier_name',
        'type',
        'data',
    ];

    protected $casts = [
        'data' => 'json',
    ];
}
