<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CartItemModifier extends Model
{
    protected $fillable = ['modifier_name', 'mod_option_name', 'rise'];
    //
}
