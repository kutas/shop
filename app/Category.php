<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    protected $fillable = ['name', 'description', 'parent_id'];

    //Relations
    public function products()
    {
        return $this->belongsToMany(Product::class);
    }

    public function modifiers()
    {
        return $this->belongsToMany(Modifier::class);
    }


    /**
     * Relation to children categories
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function children()
    {
        return $this->hasMany(Category::class, 'parent_id');
    }


    /**
     * Relation to parent category
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function parent()
    {
        return $this->belongsTo(Category::class, 'parent_id', 'id');
    }
}
