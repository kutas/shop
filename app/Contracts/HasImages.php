<?php
/**
 * Created by PhpStorm.
 * User: kutas
 * Date: 11/1/18
 * Time: 3:28 AM
 */

namespace App\Contracts;


use App\Image;
use Illuminate\Http\Request;

interface HasImages
{

    /**
     * Morph many relation
     * @return mixed
     */
    public function images();

    /**
     * Store new image
     * @param \Request $request
     * @return Image
     */
    public function addImage(Request $request, $fileName = 'image'): Image;

    /**
     * Delete image from storage and database
     * @param Image $image
     * @return mixed
     */
    public function destroyImage(Image $image);

}