<?php
/**
 * Created by PhpStorm.
 * User: kutas
 * Date: 1/13/18
 * Time: 8:46 AM
 */

namespace App\Helpers;



use App\Cart;
use App\CartItem;
use App\CartItemInput;
use App\CartItemModifier;
use App\Events\OrderCreated;
use App\Notifications\AdminOrderCreated;
use App\Notifications\ClientOrderCreated;
use App\Order;
use App\OrderStatus;
use App\User;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Session;

class CartManager
{
    protected $session;

    public function __construct(Session $session)
    {
        $this->session = $session;
    }


    public function addItem(array $requestData)
    {
        $cartItem = $this->createCartItem($requestData);

        $this->session::push('cart', $cartItem);

        return $cartItem;
    }

    public function removeItem(int $index)
    {
        $cartItems = $this->session::pull('cart');

        array_splice($cartItems,$index, 1);

        $this->session::put('cart', $cartItems);
    }

    public function setQuantity(int $index, int $quantity){

        $cart = $this->session::pull('cart');

        $cart[$index]['quantity'] = $quantity;

        $this->session::put('cart', $cart);
    }

    public function hasItems()
    {
        return !! $this->items()->count();
    }

    private function getSessionId(): string
    {
        return $this->session::getId();
    }

    public function items(): Collection
    {
        return collect($this->session::get('cart'));
    }

    public function createOrder(array $data): Order
    {

        $orderData = $this->prepareOrderData($data);

        $order = Order::create($orderData);

        $this->saveCartItemsWithModifiers($order);

        $this->session::forget('cart');

        $order->notify(new ClientOrderCreated($order));

        $admin = User::whereEmail(config('shop.admin_emails')[0])->first();


        if ($admin){
            $admin->notify(new AdminOrderCreated($order));
        }

        return $order;
    }

    private function saveCartItemsWithModifiers(Order $order){

        $this->items()->each(function($item) use ($order){
            $modifiers = $item->cartItemModifiers;
            $inputs = $item->cartItemInputs;

            unset($item->cartItemModifiers);
            unset($item->cartItemInputs);

            $order->cartItems()->save($item);

            if ($modifiers){
                $this->saveCartItemModifiers($modifiers, $item);
            }

            if ($inputs){
                $this->saveCartItemInputs($inputs, $item);
            }

        });

    }

    private function saveCartItemModifiers(Collection $modifiers, CartItem $cartItem)
    {
        $modifiers->each(function($modGroup) use ($cartItem){
            $cartItem->modifiers()->saveMany($modGroup->all());
        });
    }

    private function saveCartItemInputs(Collection $inputs, CartItem $cartItem)
    {
        $cartItem->inputs()->saveMany($inputs);
    }

    private function createCartItem(array $data): CartItem
    {
        $cartItem = CartItem::make([
            'product_id' => $data['product']['id'],
            'quantity' => $data['quantity'],
        ]);

        $cartItem->load(['product.images']);

        $cartItem->cartItemModifiers = $this->createCartItemModifiers($data['modifiers']);

        $cartItem->cartItemInputs = $this->createCartItemInputs($data['modifiers']);

        return $cartItem;
    }

    private function createCartItemModifiers(array $modifiersData): Collection
    {
        return collect($modifiersData)->map(function($modifierData){
            return collect($modifierData['options'])->map(function($option) use ($modifierData){
                return $this->createCartItemModifier($modifierData['originalModifier'], $option);
            });

        });
    }

    private function createCartItemModifier(array  $originalModifier, array $option): CartItemModifier
    {
        return CartItemModifier::make([
            'modifier_name' => $originalModifier['name'],
            'mod_option_name' => $option['name'],
            'rise' => $option['rise'],
        ]);
    }

    private function createCartItemInputs(array $modifiersData): Collection
    {
        return $this->getModifiersWithInputData($modifiersData)->map(function($modifierData){

            return $this->createCartItemInput($modifierData);

        });
    }

    private function getModifiersWithInputData(array $modifiersData): Collection
    {
        return collect($modifiersData)->filter(function($modifierData){

            return isset($modifierData['inputData']) && $modifierData['inputData'];

        });
    }

    private function createCartItemInput(array $modifierData): CartItemInput
    {
        return CartItemInput::make([
            'modifier_name' => $modifierData['originalModifier']['name'],
            'type' => $modifierData['originalModifier']['type'],
            'data' => [
                'inputData' => $modifierData['inputData'],
            ],
        ]);
    }
    private function getDefaultOrderStatus(){

        $status = OrderStatus::where('default', 1)->count() ?

            OrderStatus::where('default', 1)->first() :

            factory(OrderStatus::class)->create([
                'name' => 'Default',
                'description' => 'Default order status',
                'default' => true,
            ]);

        return $status;
    }

    private function prepareOrderData($data)
    {
        $orderData = $data['contacts'];

        $orderData['status_id'] = $this->getDefaultOrderStatus()->id;
        $orderData['shipping_id'] = $data['shipping']['model']['id'];
        $orderData['shipping_meta'] = $data['shipping']['meta'];
        $orderData['payment_id'] = $data['payment']['id'];
        $orderData['comment'] = isset($data['comment']) ? $data['comment'] : '';

        return $orderData;

    }
}