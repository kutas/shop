<?php
/**
 * Created by PhpStorm.
 * User: kutas
 * Date: 3/29/19
 * Time: 10:36 PM
 */

namespace App\Helpers;

use Artesaos\SEOTools\Traits\SEOTools as SEOToolsTrait;

class SeoManager
{
    use SEOToolsTrait;

    public function setTitle($title): self
    {
        $this->seo()->setTitle($title);

        return $this;
    }

    public function setDescription($description):self
    {
        $this->seo()->setDescription($description);

        return $this;
    }


}