<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Image;
use App\ModOption;
use Illuminate\Http\Request;

class ImagesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Image $image)
    {
        $this->validate($request, [
            'title' => 'string|nullable',
            'description' => 'string|nullable',
        ]);

        $image->update($request->all());

        return $image;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Image $image)
    {
        \Storage::disk('public')->delete($image->patch);
        $image->destroyThumbs();

        $image->delete();
    }

    public function storeForModOption(Request $request, ModOption $modOption)
    {
        $this->validate($request, [
            'mod_option_image' => 'image|required',
            'title' => 'string|nullable',
            'description' => 'string|nullable',
        ]);

        $patch = \Storage::disk('public')->put('images', $request->file('mod_option_image'));

        $image = $modOption->images()->create(array_merge($request->all(), ['patch' => $patch]));
        $image->generateThumbnail('small');
        $image->generateThumbnail('medium');

        return $image;
    }
}
