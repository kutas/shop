<?php

namespace App\Http\Controllers\Admin;

use App\Image;
use App\Modifier;
use App\ModOption;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ModOptionsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, Modifier $modifier)
    {
        $this->validate($request, [
            'name' => 'string|max:100|required',
            'description' => 'string|max:255|nullable',
            'rise' => 'numeric|required|min:0',
        ]);

        $modOption = $modifier->modOptions()->create($request->all());

        return $modOption;
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\ModOption  $modOption
     * @return \Illuminate\Http\Response
     */
    public function show(ModOption $modOption)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\ModOption  $modOption
     * @return \Illuminate\Http\Response
     */
    public function edit(ModOption $modOption)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\ModOption  $modOption
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, ModOption $modOption)
    {
        $this->validate($request, [
            'name' => 'string|max:100|required',
            'description' => 'string|max:255|nullable',
            'rise' => 'numeric|required|min:0',
        ]);

        $modOption->update($request->all());
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\ModOption  $modOption
     * @return \Illuminate\Http\Response
     */
    public function destroy(ModOption $modOption)
    {
        $modOption->images->map(function($image){
            $image->destroyThumbs();
            $image->delete();
        });

        $modOption->delete();
    }

    public function attachImage(ModOption $modOption, Image $image)
    {
        $modOption->images()->save($image);
    }
}
