<?php

namespace App\Http\Controllers\Admin;

use App\ModRule;
use App\Product;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ModRulesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }


    /**
     * Create rule for product modifiers
     *
     * @param Request $request
     * @param Product $product
     * @return \Illuminate\Http\Response
     */
    public function storeForProduct(Request $request, Product $product)
    {
        $this->validate($request, [
            'product_id' => 'numeric|min:0|exists:products,id',
            'toggle_id' => 'numeric|min:0|exists:modifiers,id',
            'toggle_option_id' => 'numeric|min:0|exists:mod_options,id',
            'target_id' => 'numeric|min:0|exists:modifiers,id',
            'action' => 'string|max:100|required',
            'config' => 'array',
            'config.min' => 'numeric|required_if:action,num_range',
            'config.max' => 'numeric|required_if:action,num_range'
        ]);

        $rule = $product->modRules()->create($request->all());

        return $rule;
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\ModRule  $modRule
     * @return \Illuminate\Http\Response
     */
    public function show(ModRule $modRule)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\ModRule  $modRule
     * @return \Illuminate\Http\Response
     */
    public function edit(ModRule $modRule)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\ModRule  $modRule
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, ModRule $modRule)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\ModRule  $modRule
     * @return \Illuminate\Http\Response
     */
    public function destroy(ModRule $modRule)
    {
        $modRule->delete();
    }


}
