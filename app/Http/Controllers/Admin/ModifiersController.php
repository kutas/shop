<?php

namespace App\Http\Controllers\Admin;

use App\Category;
use App\Modifier;
use App\Product;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Storage;

class ModifiersController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $modifiers = Modifier::all();

        if ($request->expectsJson()){
            return $modifiers;
        } else {
            return view('admin.modifiers.index')->with(compact('modifiers'));
        }

    }

    public function getForCategory(Category $category)
    {
        $modifiers = $category->modifiers()->with('modOptions')->get();

        return $modifiers;
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $this->validate($request, [
            'name' => 'string|required|max:100',
            'description' => 'string|max:255|nullable',
            'type' => 'string|max:50|required',
            'main_gallery' => 'bool|required',
            'required' => 'bool|nullable',
        ]);

        $modifier = Modifier::create($request->all());

        return $modifier;
    }

    /**
     *
     * Create new Modifier for Product
     *
     *
     * @param Request $request
     * @param Product $product
     * @return \Illuminate\Database\Eloquent\Model
     */
    public function storeForProduct(Request $request, Product $product)
    {

        $this->validate($request, [
            'name' => 'string|max:100|required',
            'description' => 'string|max:255|nullable',
            'type' => 'string|max:50|required',
            'required' => 'bool|nullable',
            'main_gallery' => 'bool|required',
        ]);

        $modifier = $product->modifiers()->create($request->all());

        return $modifier;
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Modifier  $modifier
     * @return \Illuminate\Http\Response
     */
    public function show(Modifier $modifier)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Modifier  $modifier
     * @return \Illuminate\Http\Response
     */
    public function edit(Modifier $modifier)
    {
        $modifier->load('modOptions');
        return view('admin.modifiers.edit')->with(compact('modifier'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Modifier  $modifier
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Modifier $modifier)
    {
        $this->validate($request, [
            'name' => 'string|max:100',
            'description' => 'string|max:255|nullable',
            'type' => 'string|required|max:255',
            'main_gallery' => 'bool|required',
        ]);

        $modifier->update($request->all());
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Modifier  $modifier
     * @return \Illuminate\Http\Response
     */
    public function destroy(Modifier $modifier)
    {
        $modifier->modOptions->map(function($option){
            $option->images->map(function($image){
               $image->destroyThumbs();
               Storage::disk('public')->delete($image->patch);
               $image->delete();
            });

        });
        $modifier->delete();
    }

}
