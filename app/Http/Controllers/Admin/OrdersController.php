<?php

namespace App\Http\Controllers\Admin;

use App\Order;
use App\OrderStatus;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class OrdersController extends Controller
{
    public function index()
    {
        $orders = Order::with('status')->get();

        return view('admin.orders.index')->with(compact('orders'));
    }

    public function show(Order $order)
    {
        $order->load(['cartItems' => function($q){
            $q->with('modifiers', 'inputs', 'product.images')->get();
        }]);
        $order->load('status', 'shipping', 'payment');
        $orderStatuses = OrderStatus::all();

        return view('admin.orders.show')->with(compact('order', 'orderStatuses'));
    }
}
