<?php

namespace App\Http\Controllers\Admin;

use App\Payment;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class PaymentsController extends Controller
{

    public function index()
    {
        $payments = Payment::all();
        return $payments;
    }

    public function update(Request $request, Payment $payment)
    {
        $this->validate($request, [
            'name' => 'string|max:225|required',
            'description' => 'string|max:455|required',
        ]);

        $payment->update($request->all());

        return $payment;
    }

}
