<?php

namespace App\Http\Controllers\Admin;

use App\Category;
use App\Modifier;
use App\Product;
use Illuminate\Contracts\Logging\Log;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ProductsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $products = Product::all();

        return $request->expectsJson() ? $products : view('admin.products.index')->with(compact('products'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $product = Product::create(['title' => 'New Product']);

        $product->update(['code' => $product->id]);

        return redirect()->route('products.edit', $product);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'title' => 'string|required',
            'description' => 'string|sometimes|max:65535',
            'code' => 'required',
            'price' => 'numeric|required'
        ]);


        Product::create($request->all());
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function show(Product $product)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function edit(Product $product)
    {
        $product->load(['categories', 'modifiers.modOptions.images', 'modRules', 'images']);

        $categories = Category::all();

        return view('admin.products.edit')->with(compact('product', 'categories'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Product $product)
    {

        $this->validate($request, [
            'title' => 'string|max:100|sometimes',
            'description' => 'string|sometimes|nullable|max:65535',
            'code' => 'string|max:30|sometimes|required|max:30',
            'price' => 'numeric|sometimes|required',
            'modifiers_order' => 'array|sometimes|nullable'
        ]);
        $product->update($request->all());
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function destroy(Product $product)
    {
        $product->delete();
    }


    public function categoriesSync(Request $request, Product $product)
    {
        $this->validate($request, [
            'categories' => 'array',
            'categories.*' => 'numeric',
        ]);

        $product->categories()->sync($request->categories);
    }

    public function modifierAttach(Request $request, Product $product, Modifier $modifier)
    {
        $product->modifiers()->save($modifier);
    }

    public function modifierDetach(Request $request, Product $product, Modifier $modifier)
    {
        $product->modifiers()->detach($modifier);
    }


    /**
     * Return all Modifiers for product
     *
     * @param Product $product
     *
     * @return \Illuminate\Http\Response
     */
    public function getModifiers(Product $product)
    {
        return $product->modifiers;
    }

    public function syncModifiers(Request $request, Product $product)
    {
        $this->validate($request, [
            'modifiers' => 'array',
            'modifiers.*' => 'numeric',
        ]);

        $product->modifiers()->sync($request->modifiers);
    }

    public function uploadImage(Request $request, Product $product)
    {
        $this->validate($request, [
            'product_image' => 'image|required',
        ]);

        $image = $product->images()->create([
            'patch' => $request->file('product_image')->store('images', ['disk' => 'public']),
        ]);

        $image->generateThumbnail('small');
        $image->generateThumbnail('medium');

        return $image;

    }
}
