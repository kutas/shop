<?php

namespace App\Http\Controllers\Admin;

use App\Widget;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class SettingsController extends Controller
{
    public function index()
    {
        $widgets = Widget::with('images', 'products')->get();

        return view('admin.settings.index')->with(compact('widgets'));
    }

    public function uploadSliderImage(Request $request)
    {

        $this->validate($request, [
            'image' => 'image|required',
        ]);

        $imagePath = $request->image->store('images', 'public');

    }
}
