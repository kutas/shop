<?php

namespace App\Http\Controllers\Admin;

use App\Shipping;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ShippingsController extends Controller
{
    public function index(Request $request)
    {
        $shippings = Shipping::all();

        return $shippings;
    }

    public function update(Request $request, Shipping $shipping)
    {
        $this->validate($request, [
            'name' => 'string|max:255|required',
            'description' => 'string|max:455|required',
            'meta' => 'array|nullable',
        ]);

        $shipping->update($request->all());

        return $shipping;
    }
}
