<?php

namespace App\Http\Controllers\Admin;

use App\Widget;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Storage;

class WidgetsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'type' => 'string|max:45|required',
            'title' => 'string|max:100|nullable',
            'description' => 'string|max:255|nullable',
            'template' => 'string|max:45|nullable',
        ]);

        $widget = Widget::create($request->all());

        return $widget;

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Widget $widget)
    {
        $this->validate($request, [
            'title' => 'nullable|string|max:100',
            'description' => 'nullable|string|max:255',
        ]);

        $widget->update($request->all());

        return $widget;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Widget $widget)
    {
        $widget->images->map(function ($image){
            Storage::disk('public')->delete($image->patch);
            $image->destroyThumbs();
            $image->delete();
        });
        $widget->delete();
    }

    public function storeImage(Request $request, Widget $widget)
    {
        $this->validate($request, [
            'image' => 'image|required',
        ]);

        $image = $widget->addImage($request);
        $image->generateThumbnail('medium');

        return $image;

    }

    public function syncProducts(Request $request, Widget $widget)
    {

        $this->validate($request, [
                'products_ids' => 'array',
                'products_ids.*' => 'numeric|min:0|exists:products,id|sometimes',
            ]);

        $widget->products()->sync($request->products_ids);

    }
}
