<?php

namespace App\Http\Controllers;

use App\Cart;
use App\Helpers\CartManager;
use Illuminate\Http\Request;

class CartController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Cart  $cart
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request, CartManager $cartManager)
    {
        $cartItems = $cartManager->items();

        return ($request->expectsJson()) ? $cartItems : view('cart.show')->with(compact('cartItems'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Cart  $cart
     * @return \Illuminate\Http\Response
     */
    public function edit(Cart $cart)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Cart  $cart
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Cart $cart)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Cart  $cart
     * @return \Illuminate\Http\Response
     */
    public function destroy(Cart $cart)
    {
        //
    }

    public function addItem(Request $request, CartManager $cartManager)
    {
        $this->validate($request, [
            'quantity' => 'numeric|required',
            'product.id' => 'numeric|required',
            'modifiers' => 'array',
        ]);

        return $cartManager->addItem($request->all());

    }

    public function removeItem(Request $request, CartManager $cartManager)
    {
        $this->validate($request, [
            'index' => 'numeric|required',
        ]);

        $cartManager->removeItem($request->index);
    }

    public function updateQuantity(Request $request, CartManager $cartManager)
    {
        $this->validate($request, [
            'index' => 'numeric|required',
            'quantity' => 'numeric|required',
        ]);

        $cartManager->setQuantity($request->index, $request->quantity);
    }

    /*
     * Remove cart data from session
     * */
    public function removeCartFromSession()
    {
        session()->forget('cart');
    }
}
