<?php

namespace App\Http\Controllers;

use App\Category;
use App\Helpers\SeoManager;
use Illuminate\Http\Request;

class CategoriesController extends Controller
{
    public function show(Category $category, SeoManager $seoManager)
    {
        $seoManager->setTitle($category->name)->setDescription($category->description);

        $category->load('products.images');
        //$category->load('products.images' );
        return view('categories.show')->with(compact('category'));

    }
}
