<?php

namespace App\Http\Controllers;

use App\Helpers\CartManager;
use App\Helpers\SeoManager;
use App\Payment;
use App\Shipping;
use Illuminate\Http\Request;

class CheckoutController extends Controller
{
    public function show(Request $request, CartManager $cartManager, SeoManager $seoManager)
    {

        $seoManager->setTitle('Оформление заказа');

        if ($cartManager->hasItems()){

            $cartItems =  $cartManager->items();
            $shippings = Shipping::all();
            $payments = Payment::all();

            return ($request->expectsJson()) ? $cartItems : view('checkout.show')->with(compact('cartItems', 'shippings', 'payments'));

        } else {

            return redirect()->route('cart.show');

        }

    }

}
