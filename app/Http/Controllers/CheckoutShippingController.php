<?php

namespace App\Http\Controllers;

use Carbon\Carbon;
use Illuminate\Http\Request;
use Cache;

class CheckoutShippingController extends Controller
{

    protected $cacheKeyCities = 'np-cities';

    public function getCacheCities()
    {
        return (Cache::has($this->cacheKeyCities)) ? Cache::get($this->cacheKeyCities) : response('No cached cities', 204);
    }

    public function updateCacheCities(Request $request)
    {
        $this->validate($request, [
            'np_cities' => 'required|array',
        ]);

        Cache::put($this->cacheKeyCities, $request->np_cities, Carbon::today()->addDay());
    }

    public function getCacheWarehouses(Request $request)
    {
        $this->validate($request, [
            'city_ref' => 'required|string',
        ]);

        return (Cache::has($this->getWarehousesCacheKey($request->city_ref))) ? Cache::get($this->getWarehousesCacheKey($request->city_ref)) : response('No cached warehouses for ' . $request->city_ref . ' city', 204);

    }

    public function updateCacheWarehouses( Request $request)
    {
        $this->validate($request, [
            'city_ref' => 'required|string',
            'warehouses' => 'required|array',
        ]);

        Cache::put($this->getWarehousesCacheKey($request->city_ref), $request->warehouses, Carbon::today()->addDay());
    }


    private function getWarehousesCacheKey($cityRef){
        return $this->cacheKeyCities . '.np.' . $cityRef;
    }

}
