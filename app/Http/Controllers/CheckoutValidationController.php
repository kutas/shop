<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class CheckoutValidationController extends Controller
{
    public function contacts(Request $request)
    {
        $this->validate($request, [
            'first_name' => 'string|required',
            'last_name' => 'string|required',
            'email' => 'email|required',
            'phone' => 'string|required',
        ]);
    }

    public function shipping(Request $request)
    {
        $this->validate($request, [
            'shipping_slug' => 'required|exists:shippings,slug',
            'meta.np_city_ref' => 'required_if:shipping_slug,nova_poshta|string',
            'meta.city_name' => 'required_if:shipping_slug,nova_poshta|string',
            'meta.np_warehouse_ref' => 'required_if:shipping_slug,nova_poshta|string',
            'meta.warehouse_name' => 'required_if:shipping_slug,nova_poshta|string',
        ]);
    }

    public function payment(Request $request)
    {

        $this->validate($request, [
            'payment_slug' => 'required|exists:payments,slug',
        ]);

    }
}
