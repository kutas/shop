<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class CustomerDashboardController extends Controller
{
    public function __invoke()
    {
        $user = Auth::user();
        $user->load(['orders' => function($order){
            $order->with(['cartItems' => function($query){
                $query->with('modifiers', 'inputs', 'product')->get();
            }]);
            $order->with('status', 'shipping', 'payment');
        }]);




        return view('customer_dashboard')->with(compact('user'));
    }
}
