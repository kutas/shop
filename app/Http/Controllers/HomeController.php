<?php

namespace App\Http\Controllers;

use App\Category;
use App\Widget;
use Illuminate\Http\Request;

class HomeController extends Controller
{

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $categories = Category::all();
        $widgets = Widget::with('images')->get();
        $widgets->load(['products.images']);

        return view('home')->with(compact('categories', 'widgets'));
    }
}



