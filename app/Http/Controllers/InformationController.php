<?php

namespace App\Http\Controllers;

use App\Payment;
use App\Shipping;
use Illuminate\Http\Request;

class InformationController extends Controller
{

    public function __invoke()
    {
        $shippings = Shipping::all();
        $payments = Payment::all();

        return view('shop_info')->with(compact('shippings', 'payments'));
    }
}
