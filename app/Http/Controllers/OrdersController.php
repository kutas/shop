<?php

namespace App\Http\Controllers;

use App\Helpers\CartManager;
use App\Order;
use Illuminate\Http\Request;

class OrdersController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, CartManager $cartManager)
    {
        $this->validate($request, [
            'contacts.first_name' => 'required|string|max:255',
            'contacts.last_name' => 'nullable|string|max:255',
            'contacts.email' => 'required|email',
            'contacts.phone' => 'required|string|max:255',

            'payment.id' => 'required|numeric|exists:payments,id',

            'shipping.model.id' => 'required|numeric|exists:shippings,id',
            'shipping.meta' => 'nullable|array',

            'comment' => 'nullable|string',

        ]);

        $order = $cartManager->createOrder($request->all());

        return response($order, 201);

    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Order  $order
     * @return \Illuminate\Http\Response
     */
    public function show(Order $order)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Order  $order
     * @return \Illuminate\Http\Response
     */
    public function edit(Order $order)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Order  $order
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Order $order)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Order  $order
     * @return \Illuminate\Http\Response
     */
    public function destroy(Order $order)
    {
        //
    }
}
