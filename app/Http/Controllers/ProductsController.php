<?php

namespace App\Http\Controllers;

use App\Helpers\SeoManager;
use App\Product;
use Illuminate\Http\Request;

class ProductsController extends Controller
{
    public function show(Product $product, SeoManager $seoManager)
    {
        $seoManager->setTitle($product->title)->setDescription($product->description);

        $product->load('images', 'modifiers.modOptions.images', 'modRules', 'categories');

        return view('products.show')->with(compact('product'));
    }
}
