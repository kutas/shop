<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Storage;
use Intervention\Image\ImageManagerStatic as ImageResizer;

class Image extends Model
{
    protected $fillable = [
        'patch', 'title', 'description',
    ];



    public function thumbPath(string $size = 'small') : string
    {
        $fileName = explode('/', $this->patch)[1]; //TODO: get file name with Storage helper

        return config('shop.thumbnails.path') . "/{$size}-{$fileName}";
    }

    public function generateThumbnail(string $size = 'small')
    {
        if ($this->patch && Storage::disk('public')->exists($this->patch)){

            $imageThumb = ImageResizer::make(Storage::disk('public')->get($this->patch));
            $thumbPath = "images/thumbnails/{$size}-" . explode('/',$this->patch)[1];

            $imageThumb->resize(config("shop.thumbnails.sizes.{$size}.width"), config("shop.thumbnails.sizes.{$size}.height"), function ($constraint) {
                $constraint->upsize();
                $constraint->aspectRatio();
            });

            Storage::disk('public')->put($thumbPath, $imageThumb->encode());

        }

    }

    public function destroyThumbs()
    {
        foreach (config('shop.thumbnails.sizes') as $size => $values){

            Storage::disk('public')->delete($this->thumbPath($size));
        }
    }
}