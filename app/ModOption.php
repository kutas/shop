<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ModOption extends Model
{
    protected $fillable = ['name', 'description', 'rise'];

    //Relations

    public function images()
    {
        return $this->morphMany(Image::class, 'imageable');
    }

    public function modifier()
    {
        return $this->belongsTo(Modifier::class);
    }
}
