<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ModRule extends Model
{

    protected $casts = [
        'config' => 'json',
    ];

    protected $fillable = [
        'toggle_id',
        'toggle_option_id',
        'target_id',
        'action',
        'config'
    ];

}
