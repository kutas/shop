<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Modifier extends Model
{
    protected $fillable = ['name', 'description', 'type', 'required', 'main_gallery'];

    protected $casts = [
        'required' => 'bool',
        'main_gallery' => 'bool',
    ];

    //Relations

    public function modOptions()
    {
        return $this->hasMany(ModOption::class);
    }
}
