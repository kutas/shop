<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;

class Order extends Model
{
    use Notifiable;

    protected $fillable = [
        'first_name',
        'last_name',
        'email',
        'phone',
        'status_id',
        'shipping_id',
        'shipping_meta',
        'payment_id',
        'comment',
    ];

    protected $casts = [
        'shipping_meta' => 'json',
    ];

    //Relations
    public function cart()
    {
        return $this->hasOne(Cart::class);
    }

    public function cartItems()
    {
        return $this->hasMany(CartItem::class);
    }

    public function status()
    {
        return $this->belongsTo(OrderStatus::class);
    }

    public function shipping()
    {
        return $this->belongsTo(Shipping::class);
    }

    public function payment()
    {
        return $this->belongsTo(Payment::class);
    }
}
