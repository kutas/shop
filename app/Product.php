<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    protected $fillable = [
        'title', 'description', 'code', 'price', 'modifiers_order',
    ];

    protected $casts = [
        'modifiers_order' => 'json'
    ];

    //Relations

    /**
     * Many to many relation to product categories
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function categories()
    {
        return $this->belongsToMany(Category::class);
    }

    /**
     * One to many relation to product Modifiers. Modifier can belong only to one product
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function modifiers()
    {
        return $this->belongsToMany(Modifier::class);
    }

    /**
     * One to many relation to modifiers rules. Product can have many rules for own modifiers
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function modRules()
    {
        return $this->hasMany(ModRule::class);
    }

    /**
     *  Morph one to many relation to images
     *
     * @return \Illuminate\Database\Eloquent\Relations\MorphMany
     */
    public function images()
    {
        return $this->morphMany(Image::class,'imageable');
    }

}
