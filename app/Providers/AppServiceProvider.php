<?php

namespace App\Providers;

use App\Category;
use App\Helpers\CartManager;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        \View::composer('aside.categories', function ($view){
            $currentCategory = ('categories.show' == request()->route()->getName()) ? request()->category : 'undefined';
            $categories = Category::where(['parent_id' => null])->with('children')->get();

            $view->with(compact('categories', 'currentCategory'));
        });


        \View::composer('layouts.app', function($view){
            $cartItems = $this->app->make(CartManager::class)->items();
            $user = Auth::check() ? Auth::user() : 'undefined';
            $isAdmin = Auth::check()? $user->isAdmin() : 0;

            $view->with(compact('cartItems', 'user', 'isAdmin'));
        });

        \View::composer('layouts.admin', function($view){
            $user = Auth::check() ? Auth::user() : 'undefined';
            $isAdmin = Auth::check()? $user->isAdmin() : 0;

            $view->with(compact( 'user', 'isAdmin'));
        });


    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
