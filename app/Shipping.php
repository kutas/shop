<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Shipping extends Model
{
    protected $fillable = ['name', 'description', 'slug', 'meta'];

    protected $casts = [
        'meta' => 'json',
    ];

}
