<?php
/**
 * Created by PhpStorm.
 * User: kutas
 * Date: 10/31/18
 * Time: 2:56 PM
 */

namespace App\Traits;


use App\Image;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

trait HasImages
{


    /**
     * MorphMany relation
     *
     * @return mixed
     */
    public function images()
    {
        return $this->morphMany(Image::class, 'imageable');
    }

    public function addImage(Request $request, $fileName = 'image'): Image
    {

        $imagePath = $request->file($fileName)->store('images', 'public');

        $image = $this->images()->create(['patch' => $imagePath]);

        return $image;
    }

    public function destroyImage(Image $image)
    {
        Storage::disk('public')->delete($image->patch);

        $image->delete();
    }
    
}