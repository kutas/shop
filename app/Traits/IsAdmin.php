<?php
/**
 * Created by PhpStorm.
 * User: kutas
 * Date: 11/27/18
 * Time: 1:23 AM
 */

namespace App\Traits;


trait IsAdmin
{
    public function isAdmin()
    {
        return in_array($this->email, config('shop.admin_emails'));
    }

}