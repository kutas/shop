<?php

namespace App;

use App\Contracts\HasImages;
use Illuminate\Database\Eloquent\Model;

class Widget extends Model implements HasImages
{
    use \App\Traits\HasImages;


    protected $fillable = [
        'title', 'description', 'type', 'template',
    ];

    //Relations
    public function products()
    {
        return $this->belongsToMany(Product::class, 'product_widget');
    }

}
