<?php

return [

    /*
     *
     * Admin email
    |
    */

    'admin_emails' => ['evgenij.kutas@gmail.com'],

    /*
     * Thumbnails sizes
     * */

    'thumbnails' => [
        'path' => 'images/thumbnails',
        'sizes' => [
            'small' => [
                'width' => 80,
                'height' => 120,
            ],
            'medium' => [
                'width' => 220,
                'height' => 300,
            ]
        ],

    ]
];
