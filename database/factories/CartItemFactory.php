<?php

use Faker\Generator as Faker;

$factory->define(\App\CartItem::class, function (Faker $faker) {
    return [
        'order_id' => factory(\App\Order::class)->create()->id,
        'product_id' => factory(\App\Product::class)->create()->id,
        'quantity' => 1,
    ];
});
