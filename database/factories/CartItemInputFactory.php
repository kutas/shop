<?php

use Faker\Generator as Faker;

$factory->define(\App\CartItemInput::class, function (Faker $faker) {
    $modifier = factory(\App\Modifier::class)->create();
    return [
        'cart_item_id' => factory(\App\CartItem::class)->create()->id,
        'modifier_name' => $modifier->name,
        'type' => $modifier->type,
        'data' => ['text' => $faker->sentence],
    ];
});
