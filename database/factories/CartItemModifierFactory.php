<?php

use Faker\Generator as Faker;

$factory->define(\App\CartItemModifier::class, function (Faker $faker) {
    $modOption = factory(\App\ModOption::class)->create();
    return [
        'cart_item_id' => factory(\App\CartItem::class)->create()->id,
        'modifier_name' => $modOption->modifier->name,
        'mod_option_name' => $modOption->name,
        'rise' => $modOption->rise,
    ];
});
