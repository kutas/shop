<?php

use Faker\Generator as Faker;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Facades\Storage;


$factory->define(\App\Image::class, function (Faker $faker) {
    return [
        'patch' => bcrypt('hello'),
        'title' => $faker->text(20),
        'description' => $faker->text(40),
    ];
});
