<?php

use Faker\Generator as Faker;

$factory->define(\App\ModOption::class, function (Faker $faker) {
    return [
        'modifier_id' => factory(\App\Modifier::class)->create()->id,
        'name' => $faker->name(),
        'description' => $faker->text(60),
        'rise' => $faker->numberBetween(0, 300),
    ];
});
