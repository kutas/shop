<?php

use Faker\Generator as Faker;

$factory->define(\App\ModRule::class, function (Faker $faker) {

    $product = factory(\App\Product::class)->create();
    $target_mod = factory(\App\Modifier::class)->create();
    $toggle_mod = factory(\App\Modifier::class)->create();

    $product->modifiers()->save($target_mod);
    $product->modifiers()->save($toggle_mod);


    return [
        'product_id' => $product->id,
        'toggle_id' => $toggle_mod->id,
        'toggle_option_id' => factory(\App\ModOption::class)->create(['modifier_id' => $toggle_mod->id]),
        'target_id' => $target_mod->id,
        'action' => 'hide',
        'config' => [
            'min' => $faker->numberBetween(0, 100),
            'max' => $faker->numberBetween(100, 300),
        ],
    ];
});
