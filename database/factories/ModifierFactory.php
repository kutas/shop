<?php

use Faker\Generator as Faker;

$factory->define(\App\Modifier::class, function (Faker $faker) {
    return [
        'name' => $faker->word,
        'description' => $faker->text(60),
        'type' => 'select',
        'main_gallery' => $faker->boolean(),
        'required' => false,
    ];
});
