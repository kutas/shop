<?php

use Faker\Generator as Faker;

$factory->define(\App\Order::class, function (Faker $faker) {
    $status = \App\OrderStatus::firstOrCreate(['name' => 'New']);
    $shipping = \App\Shipping::firstOrCreate(['name' => 'Курьер', 'slug' => 'courier', 'meta' => ['warehouse' => 123]]);
    $payment = \App\Payment::firstOrCreate(['name' => 'Наличными', 'slug' => 'cash']);

    return [
        'first_name' => $faker->firstName,
        'last_name' => $faker->lastName,
        'phone' => $faker->phoneNumber,
        'email' => $faker->unique()->safeEmail,
        'status_id' => $status->id,
        'shipping_id' => $shipping->id,
        'shipping_meta' => ['warehouse' => 123, 'city' => 123],
        'payment_id' => $payment->id,
        'comment' => $faker->sentence,
    ];
});
