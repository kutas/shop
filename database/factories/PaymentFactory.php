<?php

use Faker\Generator as Faker;

$factory->define(\App\Payment::class, function (Faker $faker) {
    return [
        'name' => $faker->name,
        'description' => $faker->text,
        'slug' => $faker->word . \Carbon\Carbon::now()->timestamp,
    ];
});
