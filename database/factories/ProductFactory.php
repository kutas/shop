<?php

use Faker\Generator as Faker;

$factory->define(\App\Product::class, function (Faker $faker) {
    return [
        'title' => $faker->text(15),
        'description' => $faker->text(80),
        'code' => $faker->unique()->randomNumber(),
        'price' => $faker->randomFloat(2),
        'modifiers_order' => null,
    ];
});
