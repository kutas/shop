<?php

use Faker\Generator as Faker;

$factory->define(\App\Widget::class, function (Faker $faker) {
    return [
        'type' => $faker->sentence(1),
        'title' => $faker->title,
        'description' => $faker->text(80),
        'template' => $faker->word,
    ];
});
