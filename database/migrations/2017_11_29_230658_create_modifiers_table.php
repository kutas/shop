<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateModifiersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('modifiers', function (Blueprint $table) {
            $table->increments('id');

            $table->string('name', 100);
            $table->string('description', 255)->nullable()->default(null);
            $table->string('type', 50);
            $table->boolean('main_gallery')->default(false);
            $table->boolean('required')->default(false);

            $table->timestamps();
        });

        Schema::create('mod_options', function (Blueprint $table){

            $table->increments('id');

            $table->integer('modifier_id')->unsigned();

            $table->string('name', 100);
            $table->string('description', 255)->nullable()->default(null);
            $table->integer('rise', false, true)->default(0);

            $table->foreign('modifier_id')->references('id')->on('modifiers')->onDelete('cascade');

            $table->timestamps();
        });


        Schema::create('modifier_product', function(Blueprint $table) {

            $table->integer('modifier_id')->unsigned();
            $table->integer('product_id')->unsigned();

            $table->foreign('modifier_id')->references('id')->on('modifiers')->onDelete('cascade');
            $table->foreign('product_id')->references('id')->on('products')->onDelete('cascade');

        });

        Schema::create('category_modifier', function (Blueprint $table){


            $table->integer('category_id')->unsigned();
            $table->integer('modifier_id')->unsigned();

            $table->foreign('category_id')->references('id')->on('categories')->onDelete('cascade');
            $table->foreign('modifier_id')->references('id')->on('modifiers')->onDelete('cascade');
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::disableForeignKeyConstraints();
        Schema::dropIfExists('modifiers');
        Schema::dropIfExists('mod_options');
        Schema::dropIfExists('modifier_product');
        Schema::enableForeignKeyConstraints();
    }
}
