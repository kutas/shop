<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOrdersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {

        Schema::create('order_statuses', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->text('description')->nullable()->default(null);
            $table->boolean('default')->default(false);
            $table->timestamps();
        });


        Schema::create('orders', function (Blueprint $table) {
            $table->increments('id');
            $table->string('first_name');
            $table->string('last_name')->nullable()->default(null);
            $table->string('email');
            $table->string('phone');
            $table->integer('status_id')->unsigned();
            $table->integer('shipping_id')->unsigned();
            $table->json('shipping_meta')->nullable()->default(null);
            $table->integer('payment_id')->unsigned();
            $table->string('comment', 455)->nullable()->default(null);

            $table->foreign('status_id')->references('id')->on('order_statuses');
            $table->foreign('shipping_id')->references('id')->on('shippings');
            $table->foreign('payment_id')->references('id')->on('payments');

            $table->timestamps();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::disableForeignKeyConstraints();

        Schema::dropIfExists('order_statuses');
        Schema::dropIfExists('orders');

        Schema::enableForeignKeyConstraints();
    }
}
