<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCartItemModifiersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('cart_item_modifiers', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('cart_item_id')->unsigned();

            $table->string('modifier_name');
            $table->string('mod_option_name');
            $table->integer('rise')->unsigned()->default(0);

            $table->foreign('cart_item_id')->references('id')->on('cart_items')->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('cart_item_modifiers');
    }
}
