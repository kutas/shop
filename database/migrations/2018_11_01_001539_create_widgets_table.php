<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateWidgetsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('widgets', function (Blueprint $table) {
            $table->increments('id');
            $table->string('type', 45);
            $table->string('title', 100)->nullable()->default(null);
            $table->string('description', 255)->nullable()->default(null);
            $table->string('template', 45)->nullable()->default(null);
            $table->timestamps();
        });


        Schema::create('product_widget', function (Blueprint $table){

            $table->integer('product_id')->unsigned();
            $table->integer('widget_id')->unsigned();

            $table->foreign('product_id')->references('id')->on('products')->onDelete('cascade');
            $table->foreign('widget_id')->references('id')->on('widgets')->onDelete('cascade');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::disableForeignKeyConstraints();
        Schema::dropIfExists('widgets');
        Schema::dropIfExists('product_widget');
        Schema::enableForeignKeyConstraints();
    }
}
