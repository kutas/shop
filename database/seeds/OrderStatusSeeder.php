<?php

use Illuminate\Database\Seeder;

class OrderStatusSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(\App\OrderStatus::class)->create([
            'name' => 'New',
            'description' => 'New order',
            'default' => true,
        ]);
    }
}
