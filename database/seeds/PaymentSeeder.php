<?php

use Illuminate\Database\Seeder;

class PaymentSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        $this->truncatePaymentTable();

        factory(\App\Payment::class)->create([
            'name'          => 'Cache',
            'description'   => 'Description',
            'slug'          => 'cash'
        ]);

        factory(\App\Payment::class)->create([
            'name'          => 'Carta Privat Bank',
            'description'   => 'Privat description',
            'slug'          => 'privat_cart',
        ]);
    }

    private function truncatePaymentTable(){

        Schema::disableForeignKeyConstraints();
        DB::table('payments')->truncate();
        Schema::enableForeignKeyConstraints();

    }
}
