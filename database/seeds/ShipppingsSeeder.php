<?php

use Illuminate\Database\Seeder;

class ShipppingsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        $this->truncateShippingTable();

        factory(\App\Shipping::class)->create([
            'name' => 'Courier',
            'description' => '',
            'slug' => 'courier',
            'meta' => [],
        ]);

        factory(\App\Shipping::class)->create([
            'name' => 'Nova Poshta',
            'description' => '',
            'slug' => 'nova_poshta',
            'meta' => [
                'apiKey' => '036ebb8ab6787ce2096292bf43959fc5',
            ],
        ]);
    }

    private function truncateShippingTable()
    {
        Schema::disableForeignKeyConstraints();
        DB::table('shippings')->truncate();
        Schema::enableForeignKeyConstraints();
    }
}
