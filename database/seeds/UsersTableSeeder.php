<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        $this->truncateUsersTable();

        factory(\App\User::class)->create(['email' => 'user@app.com']);
        factory(\App\User::class)->create([
            'email' => 'evgenij.kutas@gmail.com',
            'first_name' => 'Eugene',
            'last_name' => 'Kutas',
        ]);
    }

    private function truncateUsersTable()
    {
        Schema::disableForeignKeyConstraints();
        DB::table('users')->truncate();
        Schema::enableForeignKeyConstraints();
    }

}
