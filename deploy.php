<?php
namespace Deployer;

require 'recipe/laravel.php';

// Project name
set('application', 'shop');

// Project repository
set('repository', 'https://gitlab.com/kutas/shop.git');



// Shared files/dirs between deploys
add('shared_files', []);
add('shared_dirs', []);

// Writable dirs by web server
add('writable_dirs', []);

// Hosts

host('staging')
    ->hostname('104.248.135.172')
    ->set('branch', 'staging')
    ->set('deploy_path', '/var/www/shop')
    ->set('rsync_src', __DIR__)
    ->user('root')
    ->set('composer_options', 'install --verbose --prefer-dist --no-progress --no-interaction')
    ->addSshOption('UserKnownHostsFile', '/dev/null')
    ->addSshOption('StrictHostKeyChecking', 'no');


// Tasks

// [Optional] if deploy fails automatically unlock.
after('deploy:failed', 'deploy:unlock');

// Migrate database before symlink new release.

before('deploy:symlink', 'artisan:migrate');

//Remove unnecessary local files before code upload

desc('Remove unnecessary files and folders');
task('local:clear', function (){

    cd('{{current_path}}');

    run('rm -R ./node_modules');
    run('rm -R ./vendor');
    run('rm -R ./laradock-kutas-shop');
    run('rm -R ./.git');
    run('find . -name ".git*" -delete');
    //run('find . -name ".env*" -delete');

})->local();

//Copy env file for stage host
task('deploy:copy_env', function(){
    cd('{{release_path}}');
    run('cp .env.stage .env');
})->onHosts('staging');

before('deploy:vendors', 'deploy:copy_env');
//Upload fresh code

task('deploy:upload_assets', function () {
    upload("{{rsync_src}}/public/", '{{release_path}}/public');
});

after('deploy:update_code', 'deploy:upload_assets');

task('deploy:clear_assets', function(){
    cd('{{release_path}}/public');
    run('rm storage');
});

before('artisan:storage:link', 'deploy:clear_assets');


/**
 * Main task
 */
/*desc('Deploy your project');
task('deploy', [
    //'local:clear',
    'deploy:info',
    'deploy:prepare',
    'deploy:lock',
    'deploy:release',
    //'deploy:update_code',
    'deploy:upload_code',
    'deploy:shared',
    'deploy:vendors',
    'deploy:writable',
    'artisan:storage:link',
    'artisan:view:clear',
    'artisan:config:cache',
    'artisan:optimize',
    'deploy:symlink',
    'deploy:unlock',
    'cleanup',
]);*/


/*

task('deploy', [
    'local:prepare',
    'local:update_code',
    'local:vendor',
    'local:build',
    'local:env',
    //  'local:test',
    'local:clear',
    'deploy:info',
    'deploy:prepare',
    'deploy:lock',
    'deploy:release',
    'upload',
    'deploy:shared',
    'deploy:vendors',
    'deploy:writable',
    'artisan:storage:link',
    'artisan:view:clear',
    'artisan:cache:clear',
    'artisan:config:cache',
    'artisan:optimize',
    'deploy:symlink',
    'deploy:unlock',
    'cleanup',
]);*/

