
/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');

//Load Vue plugins
import VueImg from 'v-img';
import ElementUi from 'element-ui'
import 'element-ui/lib/theme-chalk/index.css'
import 'element-ui/lib/theme-chalk/display.css'
import locale from 'element-ui/lib/locale/lang/ru-RU'

//import vue-masonry-css helper
import VueMasonryCss from 'vue-masonry-css';

window.Vue = require('vue');

//Apply Vue plugins
Vue.use(VueImg);

Vue.use(ElementUi);
Vue.use(VueMasonryCss);
Vue.use(ElementUi, { locale });


//Filters
import './filters/filters'


/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */

Vue.component('example', require('./components/Example.vue').default);
Vue.component('navbar', require('./components/Navbar.vue').default);
Vue.component('admin-aside', require('./components/admin/Aside.vue').default);
Vue.component('categories-list', require('./components/sidebars/CategoriesList.vue').default);
Vue.component('category', require('./components/categories/Category.vue').default);
Vue.component('edit-product', require('./components/admin/products/EditProduct.vue').default);
Vue.component('products', require('./components/admin/products/Products.vue').default);
Vue.component('product', require('./components/products/Product.vue').default);
Vue.component('categories', require('./components/admin/categories/Categories.vue').default);
Vue.component('edit-category', require('./components/admin/categories/EditCategory').default);
Vue.component('modifiers', require('./components/admin/modifiers/Modifiers.vue').default);
Vue.component('edit-modifier', require('./components/admin/modifiers/edit/EditModifier.vue').default);
Vue.component('cart-widget', require('./components/cart/CartWidget').default);
Vue.component('cart', require('./components/cart/Cart').default);
Vue.component('checkout', require('./components/checkout/Checkout').default);
Vue.component('admin-orders', require('./components/admin/orders/AdminOrders').default);
Vue.component('breadcrumbs', require('./components/common/Breadcrumbs').default);
Vue.component('settings-page', require('./components/admin/settings/SettingsPage').default);
Vue.component('home-widgets-list', require('./components/HomeWidgetsList').default);
Vue.component('show-order', require('./components/admin/orders/ShowOrder').default);
Vue.component('customer-dashboard', require('./components/customer_dasboard/CustomerDashboard').default);


//Auth
Vue.component('login', require('./components/auth/Login').default);
Vue.component('register', require('./components/auth/Register').default);
Vue.component('forgot-password', require('./components/auth/password/Forgot').default);
Vue.component('reset-password', require('./components/auth/password/Reset').default);


/*
 * By extending the Vue prototype with a new '$bus' property
 * we can easily access our global event bus from any child component.
 */
Object.defineProperty(Vue.prototype, '$eventsBus', {
    get() {
        return this.$root.eventsBus;
    }
});

let eventsBus = new Vue({});

const app = new Vue({
    el: '#app',
    data: {
        eventsBus: eventsBus,
    }
});
