import Vue from 'vue';
import imageUrl from  './imageUrl';
import imageThumbUrl from  './imageThumbUrl';

Vue.filter('imageUrl', imageUrl);
Vue.filter('imageThumbUrl', imageThumbUrl);
