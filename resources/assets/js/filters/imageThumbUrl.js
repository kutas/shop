/**
 * Return image url or placeholder url
 * Created by kutas on 8/1/17.
 *
 */
module.exports = function (image, thumbSize = 'small') {

    if (!image){
        return '/img/product-placeholder.jpg'
    }

    let imageName = image.patch.split('/')[1];
    let basePath = image.patch.split('/')[0];

    return '/storage/images/thumbnails/' + thumbSize + '-' + imageName;

};