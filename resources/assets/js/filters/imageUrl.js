/**
 * Return image url or placeholder url
 * Created by kutas on 8/1/17.
 *
 */
module.exports = function (image, placeholderType = 'product') {

    return image ? '/storage/' + image.patch : '/img/' + placeholderType + '-placeholder.jpg';

};