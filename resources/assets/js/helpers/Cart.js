export default class Cart {

    constructor(cartItems){
        this.cartItems = cartItems;
    }

    items(){
        return this.cartItems;
    }

    getItemTotalPrice(item){
        let itemTotal = item.product.price;
        
        if (!_.isEmpty(item.cartItemModifiers)){
            itemTotal += this.getItemModifiersSumPrice(item.cartItemModifiers);
        } else if(!_.isEmpty(item.modifiers)) {
            itemTotal += this.getItemModifiersSumPrice(item.modifiers);
        }

        return itemTotal;
    }

    getItemModifiersSumPrice(modifiers){

        return _.sum(modifiers.map(mod => {

            if (_.isArray(mod)){
                return _.sum(mod.map( option => {
                    return option.rise;
                }))
            } else {
                return mod.rise;
            }

        }));
    }

    total(){
        return _.sum(this.cartItems.map(item => {
            return this.getItemTotalPrice(item) * item.quantity;
        }));
    }

    removeItem(index){
        this.cartItems.splice(index, 1);
    }

}