export default class Checkout {

    addData(data){
        for (let field in data){
            this[field] = data[field];
        }
    }

    dataForOrderCreation(){

        return _.clone(this);
    }

}