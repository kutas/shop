/**
 * Created by kutas on 5/23/17.
 */

export default class Editable {

    constructor(){
        this.item = {}
    }

    set(item){
        this.item = item;
    }

    toggle(item){
        if (this.active() && this.is(item)){

            this.cancel();

        } else {

            this.set(item);

        }
    }

    is(itemToCompare){
        return this.item.id === itemToCompare.id;
    }

    active(){
        return !_.isEmpty(this.item);
    }

    cancel(){
        this.item = {};
    }

    get(attr = false){
        return (attr) ? this.item[attr] : this.item;
    }
    get id(){
        return this.item.id;
    }

}
