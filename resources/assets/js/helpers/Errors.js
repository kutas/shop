export default class Errors {

    constructor(){
        this.errors = {};
    }

    record(errors){
        this.errors = errors;
    }

    has(field){
        return (this.errors[field]);
    }

    get(field){
        if (this.errors[field]){
            return this.errors[field][0];
        }
    }

    show(field){
        return this.has(field) ? this.get(field) : null;
    }

    clear(field){
        delete this.errors[field];
    }

    clearAll(){
        this.errors = {};
    }

}