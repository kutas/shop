import Errors from './Errors';

export default class Form {

    constructor(data, keepFields = []){
        this.originalData = data;
        this.keepFields = keepFields;

        for (let field in data){
            this[field] = data[field];
        }

        this.errors = new Errors();
        this.busy = false;

    }


    data(){
        let data = _.clone(this);
        delete data.errors;
        delete data.originalData;
        delete data.busy;
        delete data.keepFields;


        return data;
    }

    clear(){
        for (let field in this.originalData){
            this[field] = this.keepFields.includes(field) ? this[field] : this.originalData[field];
        }
    }


    post(url){
        this.busy = true;
        return new Promise((resolve, reject) => {
            axios.post(url, this.data())
                .then(res => {
                    this.busy = false;
                    this.clear();

                    resolve(res.data);
                })
                .catch(error => {
                    this.busy = false;
                    if (422 === error.response.status){
                        this.errors.record(error.response.data.errors);
                    }
                    reject(error);
                });
        });
    }

    put(url){
        this.busy = true;

        return new Promise((resolve, reject) => {
            axios.put(url, this.data())
                .then(res => {
                    this.busy = false;
                    resolve(res);
                })
                .catch(error => {
                    this.busy = false;
                    if (422 === error.response.status){
                        this.errors.record(error.response.data.errors);
                    }
                    reject(error);
                });

        });
    }
}