export default class FormModifier{

    constructor(modifier){
        this.originalModifier = modifier;
        this.selectedValue = '';
        this.inputData = '';

        this.initDefaultOption();
    }

    initDefaultOption(){
        this.selectedValue = (!_.isUndefined(_.first(this.originalModifier.mod_options))) ? _.first(this.originalModifier.mod_options).id : '';

        //If mod type id checkbox we make value array for multiple select
        if ('checkbox' === this.originalModifier.type){
            this.selectedValue = [this.selectedValue];
        }
    }

    isSupportMultiple(){
        return 'checkbox' === this.getOriginal('type');
    }

    getOriginal(fieldName){
        return this.originalModifier[fieldName];
    }

    getOptions(){
        return this.originalModifier.mod_options;
    }

    hasOptions(){

        return !! this.getOptions().length;

    }

    getRise(){
        return _.sum(this.getSelectedOption().map(option => {

            return option.rise;

        }));
    }

    getSelectedOption(){
        return this.getOptions().filter(option => {

            return (this.isSupportMultiple())

                ? this.selectedValue.includes(option.id)

                : option.id === this.selectedValue;

        })
    }

    data(){
        return {
            originalModifier: this.originalModifier,
            options: this.getSelectedOption(),
            inputData: this.inputData,
        }
    }



}