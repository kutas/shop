import FormModifier from './FormModifier';
import Referee from './Referee';

export default class FormModifiers {

    constructor (product){
        this.modifiers = {};

        this.product = product;

        this.createModifiers();

        this.applyModifiersOrder();
    }

    createModifiers(){
        this.product.modifiers.map(mod => {

            this.modifiers[mod.id] = new FormModifier(mod);
        });
    }

    get activeFormModifiers(){
        return _.filter(this.modifiers, formModifier => {
            let referee = new Referee(this.product.mod_rules, formModifier);
            return ! referee.isModifierDisabled(this) && ! referee.isModifierHidden(this);
        });
    }
    applyModifiersOrder(){
        if (!_.isEmpty(this.modifiers)){

            this.modifiers = this.product.modifiers_order.map(modId => {
                return this.modifiers[modId];
            });

        }
    }

    get activeOptionsImages(){
        let images = [];

        this.activeFormModifiers
            .filter(mod => {
                return mod.getOriginal('main_gallery');
            })
            .map(mod => {
                return mod.getSelectedOption();
            })
            .map(options => {
                return options.map(option => {
                    return option.images;
                });
            }).forEach(mod => {
            mod.forEach(optionImages => {
                optionImages.forEach(image => {

                    images.push(image);
                });
            });
        });

        return images;
    }

    data(){

        return this.activeFormModifiers.map(formModifier => {

            return formModifier.data();

        });
    }

}