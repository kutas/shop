/**
 * Created by kutas on 6/22/17.
 */
import CitiesRepository from './CitiesRepository';
import WarehousesRepository from './WarehousesRepository';
export default class NovaPoshta{
    constructor(apiKey){
        this.apiKey = apiKey;

        this.citiesRepository = new CitiesRepository(this.apiKey);
        this.city = {};

        this.warehousesRepository = new WarehousesRepository(this.apiKey);
        this.warehouse = {};

        this.citiesRepository.retrieveCities();
    }

    getCities(){
        return this.citiesRepository.cities;
    }

    setCity(city){
        this.city = city;

        this.warehousesRepository.retrieveWarehouses(this.city.Ref);
    }

    setWarehouse(warehouse){

        this.warehouse = warehouse;

    }

    removeCity(){
        this.city = {};
        this.warehouse = {};
        this.warehousesRepository.clear();
    }

    removeWarehouse(){
        this.warehouse = {};
    }

    citySelected(){
        return !_.isEmpty(this.city);
    }

    getFilteredCities(queryString){

        return queryString ? this.getCities().filter(city => {

            return (city.DescriptionRu.toLowerCase().indexOf(queryString.toLowerCase()) === 0);

        }) : this.getCities();

    }

    getFilteredWarehouses(queryString){

        return queryString ? this.getWarehouses().filter(warehouse => {

            return (warehouse.DescriptionRu.toLowerCase().indexOf(queryString.toLowerCase()) >= 0);

        }) : this.getWarehouses();

    }

    getWarehouses(){
        return this.warehousesRepository.get();
    }

}