/**
 * Created by kutas on 6/22/17.
 */
import BaseRepository from './BaseRepository';
export default class WarehousesRepositoryCache extends BaseRepository{

    constructor(apiKey, config){

        super(apiKey);

        this.config = config;

        this.warehouses = [];
    }

    get(){
        return this.warehouses;
    }

    retrieveWarehouses(city_ref){
        this.clear();

        axios.get(route('checkout.shipping.np.warehouses', {city_ref : city_ref}))
            .then(res => {
                if (204 === res.status){
                    this.getWarehouses(city_ref);
                } else {
                    this.warehouses = res.data;
                }
            })
            .catch(error => {
                console.log(error.response.data);
            })


    }

    cacheWarehouses(city_ref){
        axios.post(route('checkout.shipping.np.cache_warehouses'), {city_ref: city_ref, warehouses: this.warehouses})
            .catch(error => {
                console.log(error.response.data);
            })
    }


    getWarehouses(city_ref){
        this.removeDefaultAxiosHeaders();
        axios.post(this.baseUrl, this.createQueryData('Address', 'getWarehouses', {'CityRef': city_ref}))
            .then((response) =>{
                this.warehouses = response.data.data;
                this.cacheWarehouses(city_ref);
            })
            .catch(error => {
                console.log(error.response.data);
            });
        this.restoreDefaultAxiosHeaders();
    }

    clear(){
        this.warehouses = [];
    }

}