export default class Referee {

    constructor(rules, formModifier){
        this.rules = rules;
        this.formModifier = formModifier;
    }

    getDependentModifierRules(){
        return this.rules.filter(rule => {
            return rule.target_id === this.formModifier.getOriginal('id');
        });
    }

    getActiveModifierRules(formModifiers){

        return this.getDependentModifierRules().filter(rule => {
            let formModifier = this.getFormModifier(rule.toggle_id, formModifiers);

            return ('checkbox' === formModifier.getOriginal('type'))
                ? formModifier.selectedValue.includes(rule.toggle_option_id)
                : formModifier.selectedValue === rule.toggle_option_id;
        });

    }

    isModifierDisabled(formModifiers){
        return !! this.getActiveModifierRules(formModifiers).filter(rule => {
            return 'disable' === rule.action;
        }).length;
    }

    isModifierHidden(formModifiers){
        return !! this.getActiveModifierRules(formModifiers).filter(rule => {
            return 'hide' === rule.action;
        }).length;
    }


    getFormModifier(modifier_id, formModifiers){
        return _.find(formModifiers.modifiers, formMod => {
            return formMod.getOriginal('id')=== modifier_id;
        });
    }

}





