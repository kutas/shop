@extends('layouts.admin')

@section('content')

    <edit-category :category="{{ $category }}" :categories="{{ $categories }}"> </edit-category>

@endsection