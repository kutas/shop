@extends('layouts.admin')

@section('content')

    <categories :categories-init="{{ $categories }}" route-categories-store="{{ route('categories.store') }}"></categories>

@endsection