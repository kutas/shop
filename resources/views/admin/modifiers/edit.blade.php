@extends('layouts.admin')

@section('content')
    <edit-modifier :modifier="{{ $modifier }}"></edit-modifier>
@endsection