@extends('layouts.admin')

@section('content')
    <admin-orders :orders-init="{{ $orders }}"> </admin-orders>
@endsection