@extends('layouts.admin')

@section('content')
    <show-order :order="{{ $order }}" :is-admin="1"> </show-order>
@endsection