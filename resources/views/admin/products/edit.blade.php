@extends('layouts.admin')

@section('content')

  <edit-product :product="{{ $product }}"
                :categories-init="{{ $categories }}"
  ></edit-product>

@endsection
