@extends('layouts.admin')

@section('content')

    <products :products-init="{{ $products }}" ></products>

@endsection