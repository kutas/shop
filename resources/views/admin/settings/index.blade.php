@extends('layouts.admin')

@section('content')

    <settings-page :widgets-init="{{ $widgets }}"></settings-page>

@endsection