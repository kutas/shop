<div class="column is-3">
    <categories-list :categories="{{ $categories }}" :current-category="{{ $currentCategory }}"></categories-list>
</div>
