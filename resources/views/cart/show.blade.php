@extends('layouts.app')

@section('content')
    <div class="column">
        <cart :cart-items="{{ $cartItems }}"></cart>
    </div>
@endsection