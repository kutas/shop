@extends('layouts.app')

@section('aside')
    @include('aside.categories')
@endsection

@section('content')
    <div class="column is-9">
        <category :category="{{ $category }}"></category>
    </div>
@endsection
