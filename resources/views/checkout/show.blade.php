@extends('layouts.app')

@section('content')
    <div class="column is-12">
        <checkout
                :cart-items="{{ $cartItems }}"
                :shippings="{{ $shippings }}"
                :payments="{{ $payments }}"
        ></checkout>
    </div>
@endsection