@extends('layouts.app')

@section('content')
    <div class="column is-12">
        <customer-dashboard :user="{{ Auth::user() }}"></customer-dashboard>
    </div>
@endsection
