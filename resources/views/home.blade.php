@extends('layouts.app')

@section('aside')
    @include('aside.categories')
@endsection

@section('content')

    <div class="column is-9">
        <home-widgets-list :widgets="{{ $widgets }}"> </home-widgets-list>
    </div>

@endsection
