<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    {{-- SEO Tags--}}
    {!! SEO::generate() !!}

    {{-- Get routes with js--}}

    @routes()
    <!-- Styles -->
    <link href="{{ mix('css/app.css') }}" rel="stylesheet">

</head>
<body>


<div class="section" id="app" style="padding: 0px">

    <navbar
            :cart-items="{{ $cartItems }}"
            is-admin="{{ $isAdmin }}"
            :user="{{ $user }}"
            current-route="{{ Route::currentRouteName() }}">

    </navbar>

    <div class="container">

        @yield('breadcrumbs')

        <div class="columns">
            @yield('aside')

            @yield('content')
        </div>
    </div>
</div>

    <!-- Scripts -->
    <script src="{{ mix('js/app.js') }}"></script>
</body>
</html>
