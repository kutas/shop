@extends('layouts.app')

@section('breadcrumbs')
    <div class="columns">
        <breadcrumbs :product="{{ $product }}"></breadcrumbs>
    </div>

@endsection

@section('content')
    <div class="column is-12">
        <product :product="{{ $product }}"></product>
    </div>
@endsection
