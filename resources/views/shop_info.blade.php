@extends('layouts.app')

@section('content')

    <div class="container">
        <div class="columns">
            <div class="column is-6">
                <h2 class="title is-2">Доставка</h2>
                @foreach($shippings as $shipping)
                    <div class="box">
                        <h4 class="title is-4">{{{ $shipping['name'] }}}</h4>
                        <div>{{ $shipping['description'] }}</div>
                    </div>

                @endforeach
            </div>


            <div class="column is-6">

                <h2 class="title is-2">Оплата</h2>
                @foreach($payments as $payment)
                    <div class="box">
                        <h4 class="title is-4">{{{ $payment['name'] }}}</h4>
                        <div>{{ $payment['description'] }}</div>
                    </div>

                @endforeach
            </div>
        </div>
    </div>


@endsection
