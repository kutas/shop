<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/home', 'HomeController@index');
Route::get('/', 'HomeController@index')->name('home');

Auth::routes();

/*
 * Shop
 * */

//Categories
Route::get('categories/{category}', 'CategoriesController@show')->name('categories.show');

//Products
Route::get('products/{product}', 'ProductsController@show')->name('products.show');

//Cart
Route::get('cart', 'CartController@show')->name('cart.show');
Route::post('cart', 'CartController@addItem')->name('cart.add_item');
Route::delete('cart', 'CartController@removeItem')->name('cart.remove_item');
Route::put('cart.item_quantity', 'CartController@updateQuantity')->name('cart.update_quantity');

//Checkout
Route::get('checkout', 'CheckoutController@show')->name('checkout.show');
Route::post('checkout/shipping/np/cities', 'CheckoutShippingController@updateCacheCities')->name('checkout.shipping.np.cache_cities');
Route::get('checkout/shipping/np/cities', 'CheckoutShippingController@getCacheCities')->name('checkout.shipping.np.cities');
Route::get('checkout/shipping/np/warehouses', 'CheckoutShippingController@getCacheWarehouses')->name('checkout.shipping.np.warehouses');
Route::post('checkout/shipping/np/warehouses', 'CheckoutShippingController@updateCacheWarehouses')->name('checkout.shipping.np.cache_warehouses');

Route::post('checkout/order', 'OrdersController@store')->name('checkout.order');

Route::get('clearsession', 'CartController@removeCartFromSession');

//Checkout validation
Route::post('checkout/validation/contacts', 'CheckoutValidationController@contacts')->name('checkout.validation.contacts');
Route::post('checkout/validation/shipping', 'CheckoutValidationController@shipping')->name('checkout.validation.shipping');
Route::post('checkout/validation/payment', 'CheckoutValidationController@payment')->name('checkout.validation.payment');

//Shop info
Route::get('shop-info', 'InformationController')->name('shop.info');


Route::group(['middleware' => 'auth'], function (){

    Route::get('dashboard', 'CustomerDashboardController')->name('customer.dashboard');

});



/*
 * Admin side
 * */

Route::group(['prefix' => 'admin', 'namespace' => 'Admin', 'middleware' => ['auth', 'isAdmin']], function(){

    //admin home
    Route::get('/', 'DashboardController@adminDashboard')->name('admin.dashboard');

    //Products

    Route::resource('products', 'ProductsController', ['except' => ['show']]);
    Route::get('products/{product}/modifiers', 'ProductsController@getModifiers')->name('products.modifiers');
    Route::put('products/{product}/modifiers', 'ProductsController@syncModifiers')->name('products.sync_modifiers');
    Route::post('products/{product}/image', 'ProductsController@uploadImage')
        ->name('products.image')
        ->middleware('optimizeImages');


    //Product-modifier relations
    Route::post('products/{product}/attach_modifier/{modifier}', 'ProductsController@modifierAttach')->name('products.attach_modifier');
    Route::delete('products/{product}/detach_modifier/{modifier}', 'ProductsController@modifierDetach')->name('products.detach_modifier');


    //Product category relationship sync
    Route::post('products/{product}/categories_sync', 'ProductsController@categoriesSync')->name('products.categories_sync');

    //Modifiers
    Route::resource('modifiers', 'ModifiersController', ['except' => ['show']]);
    Route::post('modifiers/{product}', 'ModifiersController@storeForProduct')->name('modifiers.store_for_product');
    Route::get('modifiers/for-category/{category}', 'ModifiersController@getForCategory')->name('modifiers.for_category');

    //ModOptions
    Route::post('modifiers/{modifier}/mod_options', 'ModOptionsController@store')->name('mod_options.store');
    Route::resource('mod_options', 'ModOptionsController', ['except' => ['show', 'store']]);
    Route::post('mod_options/{mod_option}/images/{image}', 'ModOptionsController@attachImage')
        ->name('mod_options.attach_image')
        ->middleware('optimizeImages');

    //ModRules
    Route::resource('mod_rules', 'ModRulesController',['only' => 'destroy']);
    Route::post('mod_rules/{product}', 'ModRulesController@storeForProduct')->name('mod_rules.store_for_product');

    //Categories for administrator
    Route::resource('categories', 'CategoriesController', ['except' => ['show']]);
    Route::post('categories/{category}/create-modifier', 'CategoriesController@createModifier')->name('categories.create_modifier');


    //Images
    Route::post('mod_options/{mod_option}/image', 'ImagesController@storeForModOption')
        ->name('mod_options.image')
        ->middleware('optimizeImages');
    Route::resource('images', 'ImagesController', ['only' => ['destroy', 'update']]);

    //Orders
    Route::resource('orders', 'OrdersController', ['only' => ['show', 'index']]);

    //Settings
    Route::get('settings', 'SettingsController@index')->name('settings.index');

    //Widgets
    Route::resource('widgets', 'WidgetsController', ['only' => ['store', 'destroy', 'update']]);
    Route::post('widgets/{widget}/image', 'WidgetsController@storeImage')
        ->name('widgets.store_image')
        ->middleware('optimizeImages');
    Route::put('widgets/{widget}/sync-products', 'WidgetsController@syncProducts')->name('widgets.sync_products');

    //Shippings
    Route::resource('shippings', 'ShippingsController', ['only' => ['index', 'update']]);

    //Payments
    Route::resource('payments', 'PaymentsController', ['only' => ['index', 'update']]);


});


