<?php

namespace Tests\Browser;

use Tests\DuskTestCase;
use Laravel\Dusk\Browser;
use Illuminate\Foundation\Testing\DatabaseMigrations;

class AuthTest extends DuskTestCase
{
    /**
     * Asset login page
     *
     * @return void
     */
    public function testGetLoginPage()
    {
        $this->browse(function (Browser $browser) {
            $browser->visit('/')
                    ->assertSee('Авторизация');
        });
    }



    public function testLoginWithoutAccount()
    {
        $this->browse(function(Browser $browser){
            $browser->visit('/login')
                ->type('email', 'app@user.com')
                ->type('password', 'secret')
                ->scrollTo('.login-button')
                ->click('.login-button')
                ->waitForText('These credentials do not match our records.')
                ->assertPathIs('/login')
                ->assertSee('These credentials do not match our records.');
        });
    }

}
