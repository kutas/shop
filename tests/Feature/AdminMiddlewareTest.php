<?php

namespace Tests\Feature;

use App\User;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class AdminMiddlewareTest extends TestCase
{
    use DatabaseTransactions;

    protected $admin;
    protected $user;

    protected function setUp()
    {
        parent::setUp();

        $this->admin = factory(User::class)->create(['email' => config('shop.admin_emails')[0]]);
        $this->user = factory(User::class)->create();

    }

    public function test_redirect_if_not_admin()
    {
        $this->actingAs($this->user);

        $response = $this->get(route('admin.dashboard'));

        $response->assertRedirect('/');

    }
}
