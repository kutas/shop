<?php

namespace Tests\Feature;

use App\Helpers\CartManager;
use App\Modifier;
use App\ModOption;
use App\Product;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Illuminate\Support\Collection;
use Tests\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;

class CartTest extends TestCase
{
    use DatabaseTransactions;

    protected $product;
    protected $modifiers;
    protected $modOptions = [];


    protected function setUp()
    {
        parent::setUp();

        $this->product = factory(Product::class)->create();
        $this->modifiers = factory(Modifier::class, 2)->create();

        $this->modifiers->map(function($mod){
            $this->modOptions[$mod->id] = factory(ModOption::class,3)->create(['modifier_id' => $mod->id]);
        });

        $this->product->modifiers()->attach($this->modifiers->pluck('id'));

    }

    public function test_add_item_to_cart()
    {
        $cartRequestData = generateCartRequestData($this->product, $this->modifiers, $this->modOptions);

        $response = $this->json('POST', route('cart.add_item'), $cartRequestData);
        $response->assertStatus(200);
        $this->assertEquals($this->product->id, session()->get('cart')[0]->product->id);
    }

    public function test_remove_item_from_cart()
    {
        $cartManager = $this->app->make(CartManager::class);
        $firstProduct = factory(Product::class)->create();
        $secondProduct = factory(Product::class)->create();

        $cartManager->addItem(generateCartRequestData($secondProduct, $this->modifiers, $this->modOptions));
        $cartManager->addItem(generateCartRequestData($firstProduct, $this->modifiers, $this->modOptions));


        $response = $this->json('DELETE', route('cart.remove_item'), ['index' => 0]);

        $response->assertStatus(200);
        $this->assertEquals($firstProduct->id, session('cart')[0]->product_id);


    }


    public function test_update_item_quantity()
    {
        $cartManager = $this->app->make(CartManager::class);

        $cartManager->addItem(generateCartRequestData($this->product, $this->modifiers, $this->modOptions));

        $this->assertCount(1, session()->get('cart'));

        $response = $this->json('PUT', route('cart.update_quantity'), ['index' => 0, 'quantity' => 2]);

        $response->assertStatus(200);

        $this->assertEquals(2, session()->get('cart')[0]['quantity']);

    }

}
