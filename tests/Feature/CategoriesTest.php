<?php

namespace Tests\Feature;

use App\Category;
use App\User;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;

class CategoriesTest extends TestCase
{
    use DatabaseTransactions;

    protected function setUp()
    {
        parent::setUp();

        $this->singInAdmin();

    }

    public function test_create_category(){

        $data = factory(Category::class)->make()->toArray();

        $response = $this->json('POST', route('categories.store'), $data);

        $response->assertSuccessful();

        $this->assertDatabaseHas('categories', ['name' => $data['name']]);

    }

    public function test_update_category()
    {

        $category = factory(Category::class)->create();

        $newName = 'New name';

        $response = $this->json('PUT', route('categories.update', $category), ['name' => $newName]);

        $response->assertSuccessful();

        $this->assertDatabaseHas('categories', ['name' => $newName]);
        $this->assertDatabaseMissing('categories', ['name' => $category->name]);

    }

    public function test_delete_category()
    {

        $category = factory(Category::class)->create();

        $response = $this->json('DELETE', route('categories.destroy', $category));

        $response->assertSuccessful();

        $this->assertDatabaseMissing('categories', ['id' => $category->id]);

    }

}
