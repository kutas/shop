<?php

namespace Tests\Feature;

use App\Image;
use Illuminate\Support\Facades\Storage;
use Intervention\Image\Image as ImageResizer;
use App\User;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Http\UploadedFile;


class ImageTest extends TestCase
{
    use DatabaseTransactions;

    protected function setUp()
    {
        parent::setUp();

        $this->singInAdmin();
    }

    public function test_can_update_image()
    {
        $image = factory(Image::class)->create();
        $newData = factory(Image::class)->make()->toArray();

        $response = $this->json('PUT', route('images.update', $image), $newData);

        $response->assertSuccessful();
        $this->assertDatabaseHas('images', $newData);
    }

    public function test_request_destroy_image()
    {
        \Storage::fake('public');

        $image = factory(Image::class)->create([
            'patch' => \Storage::disk('public')->put('images', UploadedFile::fake()->image('modOptionImage.jpg')),
        ]);

        $image->generateThumbnail();

        $this->assertTrue(\Storage::disk('public')->exists($image->patch));

        $response = $this->json('DELETE', route('images.destroy', $image));

        $response->assertStatus(200);
        $this->assertDatabaseMissing('images', $image->toArray());
        $this->assertFalse(\Storage::disk('public')->exists($image->patch), 'Image still exists');
        foreach (config('shop.thumbnails.sizes') as $size => $values){
            $this->assertFalse(\Storage::disk('public')->exists($image->thumbPath($size)), 'Thumb still exists');
        }

    }
}
