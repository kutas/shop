<?php

namespace Tests\Feature;

use App\ModOption;
use App\User;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Illuminate\Support\Facades\Storage;
use Tests\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Http\UploadedFile;


class ModOptionImageTest extends TestCase
{
    use DatabaseTransactions;

    protected $modOption;

    protected function setUp()
    {
        parent::setUp();

        $this->modOption = factory(ModOption::class)->create();
        $this->singInAdmin();
    }


    public function test_create_image_for_mod_option()
    {
        \Storage::fake('public');

        $response = $this->json('POST', route('mod_options.image', $this->modOption), [
            'mod_option_image' => UploadedFile::fake()->image('image.jpg'),
            'title' => 'Hi',
            'description' => 'description',
        ]);
        $image = $this->modOption->images()->first();

        $response->assertSuccessful();

        $this->assertEquals(1, $this->modOption->images()->count());
        $this->assertDatabaseHas('images', $this->modOption->images->first()->toArray());
        foreach (config('shop.thumbnails.sizes') as $size => $values) {
            Storage::disk('public')->assertExists($image->thumbPath($size));
        }

    }

    public function test_delete_images_with_thumbs_before_option_destroy()
    {
        \Storage::fake('public');

        $response = $this->json('POST', route('mod_options.image', $this->modOption), [
            'mod_option_image' => UploadedFile::fake()->image('image.jpg'),
            'title' => 'Hi',
            'description' => 'description',
        ]);
        $image = $this->modOption->images()->first();

        $response = $this->json('DELETE', route('mod_options.destroy', $this->modOption));

        $response->assertSuccessful();
        foreach (config('shop.thumbnails.sizes') as $size => $values) {
            $this->assertFalse(Storage::disk('public')->exists($image->thumbPath($size)), 'Image or thumbs have not deleted');
        }
    }
}
