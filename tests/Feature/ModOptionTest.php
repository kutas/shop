<?php

namespace Tests\Feature;

use App\Modifier;
use App\ModOption;
use App\User;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;

class ModOptionTest extends TestCase
{
    use DatabaseTransactions;

    protected $modifier;

    protected function setUp()
    {
        parent::setUp();

        $this->modifier = factory(Modifier::class)->create();

        $this->singInAdmin();
    }


    public function test_request_create_mod_option()
    {
        $modOptionData = factory(ModOption::class)->make(['modifier_id' => $this->modifier->id])->toArray();

        $response = $this->json('POST', route('mod_options.store', ['modifier' => $this->modifier]), $modOptionData);

        $response->assertSuccessful();

        $this->assertDatabaseHas('mod_options', $modOptionData);
    }

    public function test_update_mod_option()
    {
        $modOption = factory(ModOption::class)->create();
        $newData = [
            'name' => 'new Name',
            'description' => 'new desc',
            'rise' => 1003
        ];

        $response = $this->json('PUT', route('mod_options.update', $modOption), $newData);

        $response->assertSuccessful();

        $this->assertDatabaseHas('mod_options', array_merge($newData, ['id' => $modOption->id]));
    }

    public function test_request_destroy_mod_option()
    {
        $modOption = factory(ModOption::class)->create();

        $this->assertDatabaseHas('mod_options', $modOption->toArray());

        $response = $this->json('DELETE', route('mod_options.destroy', $modOption));

        $response->assertSuccessful();

        $this->assertDatabaseMissing('mod_options', $modOption->toArray());
    }
}
