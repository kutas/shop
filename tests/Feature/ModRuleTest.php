<?php

namespace Tests\Feature;

use App\Modifier;
use App\ModOption;
use App\ModRule;
use App\Product;
use App\User;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;

class ModRuleTest extends TestCase
{
    use DatabaseTransactions;


    protected $product;
    protected $mod_target;
    protected $mod_toggle;
    protected $target_options;
    protected $toggle_options;

    protected function setUp()
    {
        parent::setUp();

        $this->product = factory(Product::class)->create();

        $this->mod_target = factory(Modifier::class)->create();
        $this->mod_toggle = factory(Modifier::class)->create();

        $this->target_options = factory(ModOption::class,2)->create(['modifier_id' => $this->mod_target->id]);
        $this->toggle_options = factory(ModOption::class,2)->create(['modifier_id' => $this->mod_toggle->id]);

        $this->product->modifiers()->save($this->mod_target);
        $this->product->modifiers()->save($this->mod_toggle);

        $this->singInAdmin();
    }


    public function test_request_create_mod_rule()
    {
        $modRuleData = factory(ModRule::class)->make([
            'product_id' => $this->product->id,
            'toggle_id' => $this->mod_toggle->id,
            'toggle_option_id' => $this->toggle_options->first()->id,
            'target_id' => $this->mod_target->id,
            'action' => 'hide',
            'config' => ['test_key' => 'test_value'],
        ]);

        $response = $this->json('POST', route('mod_rules.store_for_product', $this->product), $modRuleData->toArray());

        $response->assertSuccessful();

        $modRuleDataForCheck = $modRuleData->toArray();
        unset($modRuleDataForCheck['config']);

        $this->assertDatabaseHas('mod_rules', $modRuleDataForCheck);
    }


    public function test_request_destroy_mod_rule()
    {
        $modRule = factory(ModRule::class)->create();

        $response = $this->json('DELETE', route('mod_rules.destroy', $modRule));

        $response->assertSuccessful();

        $this->assertDatabaseMissing('mod_rules', $modRule->only('id'));
    }
}
