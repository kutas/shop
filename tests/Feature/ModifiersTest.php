<?php

namespace Tests\Feature;

use App\ModOption;
use App\Product;
use App\User;
use App\Modifier;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Facades\Storage;
use Tests\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;

class ModifiersTest extends TestCase
{

    use DatabaseTransactions;

    protected $product;

    protected function setUp()
    {
        parent::setUp();

        $this->singInAdmin();
        $this->product = factory(Product::class)->create();

    }

    public function test_request_create_modifier()
    {
        $data = factory(Modifier::class)->make()->toArray();


        $response = $this->json('POST', route('modifiers.store'), $data);

        $response->assertSuccessful();

        $this->assertDatabaseHas('modifiers', $data);

    }

    public function test_request_create_modifier_without_type_data()
    {
        $data = [
            'name' => 'asdas',
            'description' => 'test',
        ];

        $response = $this->json('POST', route('modifiers.store'), $data);

        $response->assertStatus(422);


    }

    public function test_request_delete_modifier()
    {
        $modifier = factory(Modifier::class)->create();

        $response = $this->json('DELETE', route('modifiers.destroy', $modifier));

        $response->assertSuccessful();

        $this->assertDatabaseMissing('modifiers', ['id' => $modifier->id]);
    }

    public function test_request_update_modifier()
    {
        $modifier = factory(Modifier::class)->create();
        $newData = [
            'name' => 'New title',
            'main_gallery' => true,
            'type' => 'select'
        ];

        $response = $this->json('PUT', route('modifiers.update', $modifier), $newData);

        $response->assertSuccessful();

        $this->assertDatabaseHas('modifiers', ['id' => $modifier->id, 'name' => $newData['name']]);

    }

    public function test_get_all_modifiers_in_json()
    {
        $modifiers = factory(Modifier::class, 4)->create();

        $response = $this->json('GET', route('modifiers.index'));

        $response->assertSuccessful();

        $response->assertJson($modifiers->toArray());

    }

    public function test_create_modifier_for_product(){

        $modData = factory(Modifier::class)->make()->toArray();

        $response = $this->json('POST', route('modifiers.store_for_product', $this->product), $modData);

        $response->assertSuccessful();

        $this->assertDatabaseHas('modifiers', $modData);
        $this->assertDatabaseHas('modifier_product', ['product_id' => $this->product->id]);

    }

    public function test_delete_images_and_thumbs_before_modifier_destroying()
    {
        \Storage::fake('public');
        $modifier = factory(Modifier::class)->create();
        $modOption = factory(ModOption::class)->create(['modifier_id' => $modifier->id]);


        $this->json('POST', route('mod_options.image', $modOption), [
            'mod_option_image' => UploadedFile::fake()->image('image.jpg'),
            'title' => 'Hi',
            'description' => 'description',
        ]);
        $image = $modOption->images()->first();

        foreach (config('shop.thumbnails.sizes') as $size => $values){
            Storage::disk('public')->assertExists($image->thumbPath($size));
        }

        $response = $this->json('DELETE', route('modifiers.destroy', $modifier));
        $response->assertSuccessful();

        foreach (config('shop.thumbnails.sizes') as $size => $values){
            $this->assertFalse(Storage::disk('public')->exists($image->thumbPath($size)), 'Thumb still exists');
        }

        $this->assertFalse(Storage::disk('public')->exists($image->patch), 'Image still exists');
        $this->assertDatabaseMissing('images', ['id' => $image->id]);

    }

}
