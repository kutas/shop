<?php

namespace Tests\Feature;

use Carbon\Carbon;
use Faker\Generator;
use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class NovaPoshtaCacheTest extends TestCase
{
    protected $cities;
    protected $warehouses;
    protected $citiesCacheKey;
    protected $fakeCityRef;

    protected function setUp()
    {
        parent::setUp();

        $this->citiesCacheKey = 'np-cities';
        $this->fakeCityRef = 'city-ref';

        $this->cities = [
            'one', 'two', 'three',
        ];

        $this->warehouses = [
            'one', 'two', 'three',
        ];
    }


    public function test_call_to_empty_cities_cache()
    {
        $response = $this->json('GET', route('checkout.shipping.np.cities'));

        $response->assertStatus(204);
    }

    public function test_call_to_full_cities_cache()
    {
        \Cache::put($this->citiesCacheKey, $this->cities, Carbon::today()->addDay());

        $response = $this->json('GET', route('checkout.shipping.np.cities'));

        $response->assertSuccessful();

    }

    public function test_call_to_empty_warehouses_cache()
    {
        $response = $this->json('GET', route('checkout.shipping.np.warehouses', ['city_ref' => $this->fakeCityRef]));

        $response->assertStatus(204);

    }

    public function test_put_cities_to_cache()
    {
        $this->assertFalse(\Cache::has($this->citiesCacheKey));

        $response = $this->json('POST', route('checkout.shipping.np.cache_cities'), ['np_cities' => $this->cities]);

        $response->assertStatus(200);
        $this->assertTrue(\Cache::has($this->citiesCacheKey));
    }

    public function test_to_full_warehouses_cache()
    {
        \Cache::put($this->getWarehousesCacheKey($this->fakeCityRef), $this->warehouses, Carbon::today()->addDay());

        $response = $this->json('GET', route('checkout.shipping.np.warehouses', ['city_ref' => $this->fakeCityRef]));

        $response->assertStatus(200);
    }

    public function test_put_warehouses_to_cache()
    {
        $this->assertFalse(\Cache::has($this->getWarehousesCacheKey($this->fakeCityRef)));

        $response = $this->json('POST', route('checkout.shipping.np.cache_warehouses', ['city_ref' => $this->fakeCityRef, 'warehouses' => $this->warehouses]));

        $response->assertStatus(200);
        $this->assertTrue(\Cache::has($this->getWarehousesCacheKey($this->fakeCityRef)));
    }

    private function getWarehousesCacheKey($cityRef){
        return $this->citiesCacheKey . '.np.' . $cityRef;
    }
}
