<?php

namespace Tests\Feature;

use App\CartItem;
use App\Events\OrderCreated;
use App\Helpers\CartManager;
use App\Notifications\AdminOrderCreated;
use App\Notifications\ClientOrderCreated;
use App\Order;
use App\User;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Illuminate\Support\Facades\Event;
use Illuminate\Support\Facades\Notification;
use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class OrderTest extends TestCase
{
    use DatabaseTransactions;

    protected $requestDataForNewOrderCreation;
    protected $cartItems;
    protected $cartManager;

    protected function setUp()
    {
        parent::setUp();

        $this->cartManager = $this->app->make(CartManager::class);
        $this->cartItems = factory(CartItem::class, 2)->create();

        $this->withSession(['cart' => $this->cartItems->all()]);

    }

    public function test_request_new_order_creation()
    {

        Notification::fake();
        Event::fake();

        $admin = User::firstOrCreate(['email' => config('shop.admin_emails')[0], 'first_name' => 'Eugene', 'password' => 'secret']);


        $newOrderData = generateRequestDataForNewOrderCreation();

        $response = $this->json('POST', route('checkout.order'), $newOrderData);

        $order = Order::whereEmail($newOrderData['contacts']['email'])->first();

        $response->assertSuccessful();
        $this->assertEquals(1, $order->shipping()->count());
        $this->assertEquals(1, $order->payment()->count());

        Notification::assertSentTo(
            $order,
            ClientOrderCreated::class,
            function ($notification, $channels) use ($order) {
                return $notification->order->id === $order->id;
            }
        );

        Notification::assertSentTo(
            $admin,
            AdminOrderCreated::class,
            function ($notification, $channels) use ($order) {
                return $notification->order->id === $order->id;
            }
        );

        /*Event::assertDispatched(OrderCreated::class, function ($e) use ($order) {
            return $e->order->id === $order->id;
        });*/



    }
}
