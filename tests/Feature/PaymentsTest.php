<?php

namespace Tests\Feature;

use App\Payment;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

class PaymentsTest extends TestCase
{
    use DatabaseTransactions;

    protected function setUp()
    {
        parent::setUp();

        $this->singInAdmin();
    }

    public function test_can_update_payment()
    {
        $payment = factory(Payment::class)->create();

        $newData = factory(Payment::class)->make()->toArray();

        $response = $this->json('PUT', route('payments.update', $payment), $newData);

        $response->assertSuccessful();

        unset($newData['slug']);

        $this->assertDatabaseHas('payments', $newData);
    }

}
