<?php

namespace Tests\Feature;

use App\Modifier;
use App\Product;
use App\User;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;

class ProductModifiersOrderTest extends TestCase
{
    use DatabaseTransactions;

    protected $modifiers;
    protected $product;

    protected function setUp()
    {
        parent::setUp();

        $this->modifiers = factory(Modifier::class, 3)->create();
        $this->product = factory(Product::class)->create();

        $this->singInAdmin();
    }

    public function test_update_product_modifiers_order()
    {
        $response = $this->json('PUT', route('products.update', $this->product), ['modifiers_order' => $this->modifiers->pluck('id')->toArray()]);

        $response->assertStatus(200);
        $this->assertEquals($this->modifiers->pluck('id')->toArray(), $this->product->fresh()->modifiers_order);

    }
}
