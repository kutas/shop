<?php

namespace Tests\Feature;

use App\Modifier;
use App\Product;
use App\User;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;

class ProductsTest extends TestCase
{
    use DatabaseTransactions;


    protected function setUp()
    {
        parent::setUp();

        $this->singInAdmin();
    }

    public function test_store_new_product()
    {
        $newData = factory(Product::class)->make()->toArray();

        $response = $this->json('POST', route('products.store'), $newData);

        $response->assertSuccessful();

        $this->assertDatabaseHas('products', ['title' => $newData['title']]);

    }

    public function test_use_id_as_product_code()
    {

        $response = $this->json('GET', route('products.create'));

        $response->assertRedirect();

        $product = Product::latest()->first();

        $this->assertEquals($product->id, $product->code, 'Id should be equals to code');

    }

    public function test_delete_product()
    {

        $product = factory(Product::class)->create();

        $response = $this->json('DELETE', route('products.destroy', $product));

        $response->assertStatus(200);

        $this->assertDatabaseMissing('products', ['id' => $product->id]);

    }


    public function test_update_product()
    {

        $product = factory(Product::class)->create();

        $newTitle = 'New  title';

        $response = $this->json('PUT', route('products.update', $product), ['title' => $newTitle]);

        $response->assertStatus(200);
        $this->assertDatabaseHas('products', ['title' => $newTitle]);
        $this->assertDatabaseMissing('products', ['title' => $product->title]);

    }

    public function test_create_product()
    {
        $productsCount = \DB::table('products')->count();

        $response = $this->json('GET', route('products.create'));

        $this->assertEquals($productsCount+1, \DB::table('products')->count());

        $response->assertRedirect(route('products.edit', Product::get()->last()));
    }

    public function test_get_product_modifiers()
    {
        $product = factory(Product::class)->create();
        $modifiers = factory(Modifier::class, 4)->create()->each(function($mod) use ($product){
            $product->modifiers()->save($mod);
        });

        $response = $this->json('GET', route('products.modifiers', $product));

        $response->assertStatus(200);
        $response->assertJson($modifiers->toArray());

    }

}
