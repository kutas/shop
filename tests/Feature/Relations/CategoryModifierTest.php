<?php

namespace Tests\Feature\Relations;

use App\Category;
use App\Modifier;
use App\User;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class CategoryModifierTest extends TestCase
{

    use DatabaseTransactions;

    protected $category;

    protected function setUp()
    {
        parent::setUp();

        $this->category = factory(Category::class)->create();
        $this->singInAdmin();
    }

    public function test_create_modifier_for_category()
    {
        $modifierData = factory(Modifier::class)->make()->toArray();

        $response = $this->json('POST', route('categories.create_modifier', $this->category), $modifierData);

        $response->assertSuccessful();
        $this->assertDatabaseHas('modifiers', $modifierData);
        $this->assertDatabaseHas('category_modifier', ['category_id' => $this->category->id]);

    }


}
