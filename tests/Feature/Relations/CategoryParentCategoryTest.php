<?php

namespace Tests\Feature\Relations;

use App\Category;
use App\User;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class CategoryParentCategoryTest extends TestCase
{
    use DatabaseTransactions;

    protected $categoryChild;
    protected $categoryParent;

    protected function setUp()
    {
        parent::setUp();

        $this->categoryParent = factory(Category::class)->create();
        $this->categoryChild = factory(Category::class)->create();

        $this->singInAdmin();

    }

    public function test_attach_child_category_to_parent()
    {

        $response = $this->json('PUT', route('categories.update', $this->categoryChild), ['parent_id' => $this->categoryParent->id, 'name' => 'hi']);

        $response->assertStatus(200);

        $this->assertEquals(1, $this->categoryParent->children()->count());
    }
}
