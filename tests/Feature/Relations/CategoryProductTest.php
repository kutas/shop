<?php

namespace Tests\Feature\Relations;

use App\Category;
use App\Product;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use function MongoDB\BSON\toJSON;
use Tests\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;
use App\User;

class CategoryProductTest extends TestCase
{
    use DatabaseTransactions;

    protected $product;
    protected $category_1;
    protected $category_2;


    protected function setUp()
    {
        parent::setUp();

        $this->singInAdmin();

        $this->product = factory(Product::class)->create();
        $this->category_1 = factory(Category::class)->create();
        $this->category_2 = factory(Category::class)->create();

    }

    public function test_sync_categories_with_products()
    {
        $response = $this->json('POST', route('products.categories_sync', [
            'product' => $this->product,
            'categories' => [$this->category_1->id],
        ]));

        $response->assertStatus(200);

        $this->assertEquals(1, $this->product->categories()->count());

        $response2 = $this->json('POST', route('products.categories_sync', [
            'product' => $this->product,
            'categories' => [$this->category_1->id, $this->category_2->id],
        ]));

        $response2->assertStatus(200);
        $this->assertEquals(2, $this->product->categories()->count());

        $response3 = $this->json('POST', route('products.categories_sync', [
            'product' => $this->product,
            'categories' => [],
        ]));

        $response3->assertStatus(200);
        $this->assertEquals(0, $this->product->categories()->count());
    }
}
