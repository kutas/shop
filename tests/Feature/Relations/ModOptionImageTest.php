<?php

namespace Tests\Feature\Relations;

use App\Image;
use App\ModOption;
use App\User;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Http\UploadedFile;

class ModOptionImageTest extends TestCase
{
    use DatabaseTransactions;

    protected $modOption;
    protected $image;

    protected function setUp()
    {
        parent::setUp();

        $this->modOption = factory(ModOption::class)->create();
        $this->image = factory(Image::class)->create();

        $this->singInAdmin();
    }


    public function test_attach_image_to_mod_option()
    {
        $response = $this->json('POST', route('mod_options.attach_image', [
            'mod_option' => $this->modOption,
            'image' => $this->image,
        ]));

        $response->assertStatus(200);
        $this->assertEquals(1, $this->modOption->images()->count());

    }
}

