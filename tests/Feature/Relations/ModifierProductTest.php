<?php

namespace Tests\Feature\Relations;

use App\Modifier;
use App\Product;
use App\User;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;

class ModifierProductTest extends TestCase
{
    use DatabaseTransactions;

    protected $product;
    protected $modifier;

    protected function setUp()
    {
        parent::setUp();

        $this->product = factory(Product::class)->create();
        $this->modifier = factory(Modifier::class)->create();

        $this->singInAdmin();
    }


    public function test_attach_modifier_to_product()
    {

        $response = $this->json('POST', route('products.attach_modifier', [
            'product' => $this->product,
            'modifier' => $this->modifier
        ]));

        $response->assertStatus(200);

        $this->assertDatabaseHas('modifier_product', ['product_id' => $this->product->id, 'modifier_id' => $this->modifier->id]);

    }

    public function test_detach_modifier_from_product()
    {
        $this->product->modifiers()->save($this->modifier);

        $this->assertDatabaseHas('modifier_product', ['product_id' => $this->product->id, 'modifier_id' => $this->modifier->id]);

        $response = $this->json('DELETE', route('products.detach_modifier', [
            'product' => $this->product,
            'modifier' => $this->modifier,
        ]));

        $response->assertStatus(200);

        $this->assertDatabaseMissing('modifier_product', ['product_id' => $this->product->id, 'modifier_id' => $this->modifier->id]);

    }

    public function test_sync_modifiers_to_product()
    {
        $modifiers = factory(Modifier::class, 3)->create();

        $response = $this->json('PUT', route('products.sync_modifiers', $this->product), [
            'modifiers' => $modifiers->pluck('id'),
        ]);

        $response->assertStatus(200);
        $this->assertEquals(3, $this->product->modifiers()->count());

        $response2 = $this->json('PUT', route('products.sync_modifiers', $this->product), [
            'modifiers' => [$modifiers->first()->id],
        ]);

        $response2->assertStatus(200);
        $this->assertEquals(1, $this->product->modifiers()->count());
    }

}
