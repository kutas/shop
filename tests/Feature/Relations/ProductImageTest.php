<?php

namespace Tests\Feature\Relations;

use App\Product;
use App\User;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Http\UploadedFile;
class ProductImageTest extends TestCase
{
    use DatabaseTransactions;

    protected $product;


    protected function setUp()
    {
        parent::setUp();

        $this->product = factory(Product::class)->create();

        $this->singInAdmin();
    }


    public function test_upload_image_for_product()
    {
        \Storage::fake('public');

        $data = [
            'product_image' => UploadedFile::fake()->image('product_image.jpeg'),
        ];

        $response = $this->json('POST', route('products.image', $this->product), $data);

        $response->assertSuccessful();

        \Storage::disk('public')->assertExists($this->product->images->first()->patch);
        $this->assertEquals(1, $this->product->images()->count());
    }
}
