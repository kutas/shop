<?php

namespace Tests\Feature\Relations;

use App\Product;
use App\Widget;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class ProductWidgetTest extends TestCase
{
    use DatabaseTransactions;

    protected $products;
    protected $widget;

    protected function setUp()
    {
        parent::setUp();

        $this->products = factory(Product::class, 2)->create();
        $this->widget = factory(Widget::class)->create();

        $this->singInAdmin();

    }

    public function test_sync_products_to_widget()
    {
        //Attach products
        $response = $this->json('PUT', route('widgets.sync_products', $this->widget), ['products_ids' => $this->products->pluck('id')]);

        $response->assertSuccessful();
        $this->assertCount(2, $this->widget->products);

        //Detach products if send empty array
        $response = $this->json('PUT', route('widgets.sync_products', $this->widget), ['products_ids' => []]);

        $response->assertSuccessful();
        $this->assertCount(0, $this->widget->fresh()->products);


    }

}