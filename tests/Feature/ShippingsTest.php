<?php

namespace Tests\Feature;

use App\Shipping;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class ShippingsTest extends TestCase
{
    use DatabaseTransactions;

    protected function setUp()
    {
        parent::setUp();
        $this->singInAdmin();
    }


    public function test_can_update_shipping()
    {
        $shipping = factory(Shipping::class)->create();
        $newData = factory(Shipping::class)->make()->toArray();

        $response = $this->json('PUT', route('shippings.update', $shipping), $newData);

        $response->assertSuccessful();
        unset($newData['meta']);
        $this->assertDatabaseHas('shippings', $newData);

    }

}
