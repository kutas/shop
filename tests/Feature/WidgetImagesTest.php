<?php

namespace Tests\Feature;

use App\Image;
use App\Widget;
use Illuminate\Http\UploadedFile;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Illuminate\Support\Facades\Storage;
use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class WidgetImagesTest extends TestCase
{
    use DatabaseTransactions;

    protected function setUp()
    {
        parent::setUp();

        $this->singInAdmin();
    }

    public function test_store_widget_image()
    {

        Storage::fake('public');
        $widget = factory(Widget::class)->create();

        $response = $this->json('POST', route('widgets.store_image', $widget), [
            'image' => UploadedFile::fake()->image('test.jpg'),
        ]);

        $response->assertSuccessful();
        Storage::disk('public')->assertExists($widget->images()->first()->patch);

    }

    public function test_delete_images_before_widget_destroy()
    {
        Storage::fake('public');
        $widget = factory(Widget::class)->create();

        $response = $this->json('POST', route('widgets.store_image', $widget), [
            'image' => UploadedFile::fake()->image('test.jpg'),
        ]);

        $imageToDestroy = $widget->images()->first();

        $response = $this->json('DELETE', route('widgets.destroy', $widget));
        $response->assertSuccessful();
        $this->assertDatabaseMissing('images', ['imageable_id' => $widget->id]);
        $this->assertFalse(Storage::disk('public')->exists($imageToDestroy->patch));


        foreach (config('shop.thumbnails.sizes') as $size => $values){

            $this->assertFalse(Storage::disk('public')->exists($imageToDestroy->thumbPath($size)));
        }

    }
}
