<?php

namespace Tests\Feature;

use App\User;
use App\Widget;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class WidgetsTest extends TestCase
{
    use DatabaseTransactions;

    protected function setUp()
    {
        parent::setUp();

        $this->singInAdmin();

    }

    public function test_json_create_widget()
    {
        $widgetData = factory(Widget::class)->make()->toArray();

        $response = $this->json('POST', route('widgets.store'), $widgetData);

        $response->assertSuccessful();

        $this->assertDatabaseHas('widgets', $widgetData);
    }

    public function test_json_destroy_widget()
    {
        $widget = factory(Widget::class)->create();

        $response = $this->json('DELETE', route('widgets.destroy', $widget));

        $response->assertSuccessful();
        $this->assertDatabaseMissing('widgets', ['id' => $widget->id]);
    }

    public function test_json_update_widget()
    {
        $widget = factory(Widget::class)->create();
        $newData = factory(Widget::class)->make()->toArray();

        $response = $this->json('PUT',route('widgets.update', $widget), $newData);

        $response->assertSuccessful();
        $this->assertDatabaseHas('widgets', $newData);

    }
}
