<?php
/**
 * Created by PhpStorm.
 * User: kutas
 * Date: 1/14/18
 * Time: 12:21 PM
 */

use Faker\Generator as Faker;
/*
 * Create data for "Add new item to cart" request.
 * */
function generateCartRequestData(\App\Product $product, \Illuminate\Support\Collection $modifiers, array $modOptions): array
{
    return [
        'quantity' => 1,
        'product' => $product->toArray(),
        'modifiers' => $modifiers->map(function($mod) use ($modOptions){
            $options = ('checkbox' == $mod->type) ? collect($modOptions[$mod->id]) : collect($modOptions[$mod->id][0]);

            return  [
                'originalModifier' => $mod->toArray(),
                'options' => ('checkbox' == $mod->type) ? $options->toArray() : [$options->toArray()],
                'inputData' => ('text' == $mod->type) ? 'test ext' : '',
            ];

        })->toArray(),
    ];
}


/*
 * Create data for "Create new Order" request as final checkout step
 * */
function generateRequestDataForNewOrderCreation(): array
{

    $faker = app()->make(Faker::class);
    $payment = factory(\App\Payment::class)->create();
    $shipping = factory(\App\Shipping::class)->create();

    return [
        'contacts' => [
            'first_name' => $faker->firstName,
            'last_name' => $faker->lastName,
            'email' => $faker->email,
            'phone' => $faker->phoneNumber,
        ],
        'payment' => $payment,
        'shipping' => [
            'model' => $shipping,
            'meta' => [
                'city_name' => $faker->city,
            ]
        ],
        'comment' => $faker->text(60),
    ];

}