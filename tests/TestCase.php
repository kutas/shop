<?php

namespace Tests;

use App\User;
use Illuminate\Foundation\Testing\TestCase as BaseTestCase;

abstract class TestCase extends BaseTestCase
{
    use CreatesApplication;


    public function singIn($user = null)
    {
        $user = $user ?: factory(User::class)->create();

        $this->actingAs($user);

        return $this;
    }

    public function singInAdmin()
    {
        $admin = User::where('email', config('shop.admin_emails')[0])->first();

        if (!$admin){
            $admin = factory(User::class)->create(['email' => config('shop.admin_emails')[0]]);
        }

        return $this->singIn($admin);
    }
}
