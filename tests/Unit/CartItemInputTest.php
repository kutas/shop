<?php

namespace Tests\Unit;

use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use App\CartItemInput;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class CartItemInputTest extends TestCase
{
    use DatabaseTransactions;

    protected $CartItemInput;


    protected function setUp()
    {
        parent::setUp();

        $this->CartItemInput = factory(CartItemInput::class)->create();

    }

    public function test_cart_item_input_factory()
    {
        $this->assertInstanceOf(CartItemInput::class, $this->CartItemInput);
        $this->assertDatabaseHas('cart_item_inputs', ['id' => $this->CartItemInput->id]);

    }

}
