<?php

namespace Tests\Unit;

use App\CartItem;
use App\CartItemModifier;
use App\ModOption;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;

class CartItemModifierTest extends TestCase
{
   use DatabaseTransactions;

   protected $cartItem;
   protected $modOption;

   protected function setUp()
   {
       parent::setUp();

       $this->cartItem = factory(CartItem::class)->create();
       $this->modOption = factory(ModOption::class)->create();
   }


    public function test_create_cart_item_modifier()
    {
        $data = [
            'modifier_name' => $this->modOption->modifier->name,
            'mod_option_name' => $this->modOption->name,
            'rise' => $this->modOption->rise,
        ];

        $cartItemModifier = $this->cartItem->modifiers()->create($data);

        $this->assertInstanceOf(CartItemModifier::class, $cartItemModifier);
        $this->assertDatabaseHas('cart_item_modifiers', $data);

    }
}
