<?php

namespace Tests\Unit;

use App\Cart;
use App\CartItem;
use App\CartItemInput;
use App\Modifier;
use App\ModOption;
use App\Order;
use App\Product;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;

class CartItemTest extends TestCase
{
    use DatabaseTransactions;

    protected $product;
    protected $modifiers;
    protected $modOptions = [];
    protected $order;


    protected function setUp()
    {
        parent::setUp();

        $this->product = factory(Product::class)->create();
        $this->modifiers = factory(Modifier::class, 3)->create();

        $this->modifiers->each(function($mod) {
            $this->modOptions[$mod->id] = factory(ModOption::class, 3)->create(['modifier_id' => $mod->id]);
        });

        $this->product->modifiers()->attach($this->modifiers->pluck('id'));
        $this->order = factory(Order::class)->create();

    }

    public function test_create_cart_item()
    {
        $cartItemData = [
            'product_id' => $this->product->id,
            'quantity' => 1,
        ];

        $cartItem = $this->order->cartItems()->create($cartItemData);

        $this->assertInstanceOf(CartItem::class, $cartItem);
        $this->assertDatabaseHas('cart_items', $cartItemData);
    }

    public function test_create_cart_item_input_with_cart_item()
    {
        $cartItemInputData = factory(CartItemInput::class)->make()->toArray();
        $cartItem = factory(CartItem::class)->create();

        unset($cartItemInputData['cart_item_id']);

        $cartItem->inputs()->create($cartItemInputData);

        unset($cartItemInputData['data']);
        $this->assertDatabaseHas('cart_item_inputs', $cartItemInputData);

        $this->assertEquals(1, $cartItem->inputs()->count());


    }
}
