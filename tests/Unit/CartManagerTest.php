<?php

namespace Tests\Unit;

use App\Cart;
use App\CartItem;
use App\CartItemModifier;
use App\Events\OrderCreated;
use App\Mail\OrderCreated as OrderCreatedMail;
use App\Helpers\CartManager;
use App\Modifier;
use App\ModOption;
use App\Notifications\AdminOrderCreated;
use App\Notifications\ClientOrderCreated;
use App\Order;
use App\Product;
use App\User;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Event;
use Illuminate\Support\Facades\Notification;
use Tests\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;

class CartManagerTest extends TestCase
{
    use DatabaseTransactions;

    protected $product;
    protected $modifiers;
    protected $modOptions = [];
    protected $cartManager;
    protected $requestDataForNewCartItem;

    protected function setUp()
    {
        parent::setUp();

        $this->product = factory(Product::class)->create();
        $this->modifiers = factory(Modifier::class, 2)->create();
        $this->modifiers->push(factory(Modifier::class)->create(['type' => 'checkbox']));
        $this->modifiers->push(factory(Modifier::class)->create(['type' => 'text']));

        //Create mdOptions from all product modifiers
        $this->modifiers->each(function($mod){

            $this->modOptions[$mod->id] = factory(ModOption::class, 3)->create(['modifier_id' => $mod->id]);

        });

        //Attach modifiers to product;
        $this->product->modifiers()->attach($this->modifiers->pluck('id'));

        $this->cartManager = $this->app->make(CartManager::class);

        $this->requestDataForNewCartItem = generateCartRequestData($this->product, $this->modifiers,$this->modOptions);


    }


    public function test_create_new_cart_manager()
    {

        $this->assertInstanceOf(CartManager::class, $this->cartManager);

    }
    
    public function test_add_cart_item_to_cart()
    {


        $cartItem = $this->cartManager->addItem($this->requestDataForNewCartItem);

        $this->assertInstanceOf(CartItem::class, $cartItem);
        $this->assertEquals($this->requestDataForNewCartItem['quantity'], session()->get('cart')[0]['quantity']);
        $this->assertInstanceOf(Product::class, $cartItem->product);
        $this->assertCount(1, session('cart'));

    }

    public function test_delete_item_from_cart()
    {

        $index = 0;
        $secondProduct = factory(Product::class)->create();

        $this->cartManager->addItem(generateCartRequestData($this->product, $this->modifiers, $this->modOptions));
        $this->cartManager->addItem(generateCartRequestData($secondProduct, $this->modifiers, $this->modOptions));

        $this->assertCount(2, session()->get('cart'));

        $this->cartManager->removeItem($index);

        $this->assertCount(1, session()->get('cart'));
        $this->assertEquals($secondProduct->id, session()->get('cart')[0]->product_id);

    }

    public function test_get_cart_items()
    {

        $this->cartManager->addItem($this->requestDataForNewCartItem);

        $cartItems = $this->cartManager->items();

        $this->assertInstanceOf(Collection::class, $cartItems);
        $this->assertEquals(1, $cartItems->count());
    }

    public function test_set_item_quantity()
    {
        $this->cartManager->addItem($this->requestDataForNewCartItem);

        $this->assertCount(1, session()->get('cart'));

        $this->cartManager->setQuantity(0, 2);

        $this->assertEquals(2, session()->get('cart')[0]['quantity']);
    }

    public function test_has_items()
    {
        $this->cartManager->addItem($this->requestDataForNewCartItem);

        $this->assertTrue($this->cartManager->hasItems());
    }

    public function test_create_new_order_with_product_with_modifiers()
    {

        Event::fake();

        Notification::fake();

        $admin =User::firstOrCreate(['email' => config('shop.admin_emails')[0], 'first_name' => 'Eugene', 'password' => 'secret']);

        $this->cartManager->addItem($this->requestDataForNewCartItem);

        $order = $this->cartManager->createOrder(generateRequestDataForNewOrderCreation());

        $this->assertInstanceOf(Order::class, $order);
        $this->assertDatabaseHas('orders', ['id' => $order->id]);
        $this->assertEquals(1, $order->cartItems()->count());
        $this->assertEquals(1, $order->cartItems()->first()->inputs()->count());
        $this->assertFalse($this->cartManager->hasItems());
        $this->assertFalse(session()->has('cart'));

        Notification::assertSentTo(
            $order,
            ClientOrderCreated::class,
            function ($notification, $channels) use ($order) {
                return $notification->order->id === $order->id;
            }
        );

        Notification::assertSentTo(
            $admin,
            AdminOrderCreated::class,
            function ($notification, $channels) use ($order) {
                return $notification->order->id === $order->id;
            }
        );


    }

    public function test_create_new_order_with_product_without_modifiers()
    {
        $cartItem = factory(CartItem::class)->create();
        $this->withSession(['cart' => [$cartItem]]);

        $order = $this->cartManager->createOrder(generateRequestDataForNewOrderCreation());

        $this->assertInstanceOf(Order::class, $order);
        $this->assertDatabaseHas('orders', ['id' => $order->id]);
        $this->assertEquals(1, $order->cartItems()->count());
        $this->assertFalse($this->cartManager->hasItems());
        $this->assertFalse(session()->has('cart'));
    }

}
