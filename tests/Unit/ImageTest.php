<?php

namespace Tests\Unit;

use App\Image;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Facades\Storage;
use Tests\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;

class ImageTest extends TestCase
{
    use DatabaseTransactions;

    public function test_create_image()
    {
        $image = factory(Image::class)->create();

        $this->assertDatabaseHas('images', $image->toArray());
    }

    public function test_get_thumbnail_path()
    {
        $thumbSize = 'small';
        $imageModel = factory(Image::class)->create([
            'patch' => Storage::disk('public')->put('images', UploadedFile::fake()->image('modOptionImage.jpg', 200, 400)),
        ]);
        $imageName = explode('/', $imageModel->patch)[1];

        $this->assertEquals(config('shop.thumbnails.path') . "/$thumbSize-$imageName", $imageModel->thumbPath($thumbSize));
    }

    public function test_image_can_has_thumbnail()
    {
        Storage::fake('public');


        $imageModel = factory(Image::class)->create([
            'patch' => Storage::disk('public')->put('images', UploadedFile::fake()->image('modOptionImage.jpg', 200, 400)),
        ]);



        foreach (config('shop.thumbnails.sizes') as $size => $values){
            $imageModel->generateThumbnail($size);
        }

        foreach (config('shop.thumbnails.sizes') as $size => $values){
            Storage::disk('public')->assertExists($imageModel->thumbPath($size));
        }


    }

    public function test_destroy_images_thumbs()
    {
        Storage::fake('public');

        $imageModel = factory(Image::class)->create([
            'patch' => Storage::disk('public')->put('images', UploadedFile::fake()->image('modOptionImage.jpg', 200, 400)),
        ]);

        foreach (config('shop.thumbnails.sizes') as $size => $values){
            $imageModel->generateThumbnail($size);
        }

        foreach (config('shop.thumbnails.sizes') as $size => $values){
            Storage::disk('public')->assertExists($imageModel->thumbPath($size));
        }

        $imageModel->destroyThumbs();

        foreach (config('shop.thumbnails.sizes') as $size => $values){
            $this->assertFalse(Storage::disk('public')->exists($imageModel->thumbPath($size)));
        }




    }


}
