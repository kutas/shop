<?php

namespace Tests\Unit;

use App\Modifier;
use App\ModOption;
use App\Product;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;

class ModRuleTest extends TestCase
{
    use DatabaseTransactions;

    protected $product;
    protected $modifier_toggle;
    protected $modifier_target;
    protected $toggle_options;
    protected $target_options;


    protected function setUp()
    {
        parent::setUp();

        $this->product = factory(Product::class)->create();

        $this->modifier_toggle = factory(Modifier::class)->create(['name' => 'toggle']);
        $this->modifier_target = factory(Modifier::class)->create(['name' => 'target']);
        $this->toggle_options = factory(ModOption::class,2)->create(['modifier_id' => $this->modifier_toggle->id]);
        $this->target_options = factory(ModOption::class,2)->create(['modifier_id' => $this->modifier_target->id]);

        $this->product->modifiers()->save($this->modifier_target);
        $this->product->modifiers()->save($this->modifier_toggle);

    }


    public function test_product_has_modifiers()
    {
        $ruleData = [
            'toggle_id' => $this->modifier_toggle->id,
            'toggle_option_id' => $this->toggle_options->first()->id,
            'target_id' => $this->modifier_target->id,
            'action' => 'hidden',
        ];
        $this->product->modRules()->create($ruleData);

        $this->assertDatabaseHas('mod_rules', array_merge($ruleData, ['product_id' => $this->product->id]));

    }

}
