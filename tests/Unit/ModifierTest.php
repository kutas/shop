<?php

namespace Tests\Unit;

use App\Modifier;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class ModifierTest extends TestCase
{

    use DatabaseTransactions;

    protected $modifier;

    protected function setUp()
    {
        parent::setUp();

        $this->modifier = factory(Modifier::class)->create();
    }

    public function test_create_modifier()
    {
        $this->assertInstanceOf(Modifier::class, $this->modifier);
    }

    public function test_modifier_can_be_required()
    {
        $this->assertFalse($this->modifier->required);
    }
}
