<?php

namespace Tests\Unit;

use App\OrderStatus;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class OrderStatusTest extends TestCase
{
    use DatabaseTransactions;

    protected $orderStatus;

    protected function setUp()
    {
        parent::setUp();

        $this->orderStatus = factory(OrderStatus::class)->create();

    }

    public function test_order_status_creation()
    {
        $this->assertDatabaseHas('order_statuses', ['id' => $this->orderStatus->id]);
    }
}
