<?php

namespace Tests\Unit;

use App\Cart;
use App\CartItem;
use App\Helpers\CartManager;
use App\Order;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class OrderTest extends TestCase
{
    use DatabaseTransactions;

    protected $requestDataForNewOrderCreation;
    protected $cartItems;


    protected function setUp()
    {

        parent::setUp();

        $this->cartItems = factory(CartItem::class, 2)->make();
        $this->requestDataForNewOrderCreation = generateRequestDataForNewOrderCreation();

        $this->withSession(['cart' => $this->cartItems->all()]);
    }

    public function test_new_order_creation()
    {

        $order = factory(Order::class)->create();

        $this->assertInstanceOf(Order::class, $order);
        $this->assertDatabaseHas('orders', ['id' => $order->id]);
    }



}
