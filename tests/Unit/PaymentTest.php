<?php

namespace Tests\Unit;

use App\Payment;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class PaymentTest extends TestCase
{
    use DatabaseTransactions;

    protected $payment;

    protected function setUp()
    {
        parent::setUp();

        $this->payment = factory(Payment::class)->create();
    }

    public function test_payment_factory_creation()
    {
        $this->assertInstanceOf(Payment::class, $this->payment);

        $this->assertDatabaseHas('payments', ['description' => $this->payment->description]);
    }
}
