<?php

namespace Tests\Unit\Relations;

use App\CartItem;
use App\CartItemInput;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class CartItemCartItemInputTest extends TestCase
{

    use DatabaseTransactions;


    protected $cartItem;
    protected $cartItemInput;


    protected function setUp()
    {
        parent::setUp();

        $this->cartItem = factory(CartItem::class)->create();
        $this->cartItemInput = factory(CartItemInput::class)->create(['cart_item_id' => $this->cartItem->id]);
    }

    public function test_cart_item_has_cart_item_inputs()
    {
        $this->assertEquals(1, $this->cartItem->inputs()->count());
    }
}
