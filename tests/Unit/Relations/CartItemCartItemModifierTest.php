<?php

namespace Tests\Unit\Relations;

use App\CartItem;
use App\CartItemModifier;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;

class CartItemCartItemModifierTest extends TestCase
{
    use DatabaseTransactions;

    protected $cartItem;
    protected $cartItemModifier;

    protected function setUp()
    {
        parent::setUp();

        $this->cartItem = factory(CartItem::class)->create();
        $this->cartItemModifier = factory(CartItemModifier::class)->create(['cart_item_id' => $this->cartItem->id]);

    }

    public function test_cart_item_has_cart_item_modifiers()
    {
        $this->assertEquals(1, $this->cartItem->modifiers()->count());
    }
}
