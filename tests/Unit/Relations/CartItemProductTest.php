<?php

namespace Tests\Unit\Relations;

use App\CartItem;
use App\Product;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;

class CartItemProductTest extends TestCase
{
    use DatabaseTransactions;

    protected $cartItem;

    protected function setUp()
    {
        parent::setUp();

        $this->cartItem = factory(CartItem::class)->create();
    }

    public function test_cart_item_has_product()
    {
        $this->assertEquals(1, $this->cartItem->product()->count());
        $this->assertEquals($this->cartItem->product_id, $this->cartItem->product->id);
    }
}
