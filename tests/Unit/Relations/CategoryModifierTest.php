<?php

namespace Tests\Unit\Relations;

use App\Category;
use App\Modifier;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class CategoryModifierTest extends TestCase
{
    use DatabaseTransactions;

    protected $category;
    protected $modifier;

    protected function setUp()
    {
        parent::setUp();

        $this->category = factory(Category::class)->create();
        $this->modifier = factory(Modifier::class)->create();

    }


    public function test_category_has_modifier()
    {
        $this->category->modifiers()->attach($this->modifier);

        $this->assertEquals(1, $this->category->modifiers()->count());
    }
}
