<?php

namespace Tests\Unit\Relations;

use App\Category;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class CategoryParentCategoryTest extends TestCase
{

    use DatabaseTransactions;

    protected $categoryChildren;
    protected $categoryParent;

    protected function setUp()
    {
        parent::setUp();

        $this->categoryParent = factory(Category::class)->create();
        $this->categoryChildren = factory(Category::class, 2)->create(['parent_id' => $this->categoryParent->id]);


    }

    public function test_category_has_children_category()
    {
        $this->assertEquals(2, $this->categoryParent->children()->count());

    }

    public function test_category_has_parent()
    {
        $this->assertEquals(1, $this->categoryChildren->first()->parent()->count());
    }


}
