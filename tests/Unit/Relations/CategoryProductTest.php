<?php

namespace Tests\Unit\Relations;

use App\Category;
use App\Product;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;

class CategoryProductTest extends TestCase
{
    use DatabaseTransactions;

    protected $product;
    protected $category;

    protected function setUp()
    {
        parent::setUp();

        $this->product = factory(Product::class)->create();
        $this->category = factory(Category::class)->create();

    }


    public function test_product_belongs_to_category()
    {
        $this->product->categories()->sync([$this->category->id]);

        $this->assertDatabaseHas('category_product', ['product_id' => $this->product->id, 'category_id' => $this->category->id]);
        $this->assertEquals(1,$this->category->products()->count());
        $this->assertEquals(1,$this->product->categories()->count());

    }


}