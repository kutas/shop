<?php

namespace Tests\Unit\Relations;

use App\Image;
use App\ModOption;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;

class ModOptionImageTest extends TestCase
{
    use DatabaseTransactions;

    protected $image;
    protected $modOption;

    protected function setUp()
    {
        parent::setUp();

        $this->image = factory(Image::class)->create();
        $this->modOption = factory(ModOption::class)->create();
    }

    public function test_mod_option_has_image()
    {
        $this->modOption->images()->save($this->image);
        $this->assertEquals(1, $this->modOption->images()->count());
        $this->assertEquals($this->image->id, $this->modOption->images()->first()->id);
    }
}
