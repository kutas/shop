<?php

namespace Tests\Unit\Relations;

use App\ModOption;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;

class ModOptionModifierTest extends TestCase
{

    use DatabaseTransactions;

    protected $modOption;

    protected function setUp()
    {
        parent::setUp();

        $this->modOption = factory(ModOption::class)->create();
    }


    public function test_mod_option_has_modifier(){

        $this->assertEquals(1, $this->modOption->modifier()->count());

    }
}
