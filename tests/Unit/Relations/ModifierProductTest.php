<?php

namespace Tests\Unit\Relations;

use App\Modifier;
use App\Product;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;

class ModifierProductTest extends TestCase
{
    use DatabaseTransactions;

    protected $product;
    protected $modifier;

    protected function setUp()
    {
        parent::setUp();

        $this->product = factory(Product::class)->create();
        $this->modifier = factory(Modifier::class)->create();
    }


    public function test_modifier_belongs_to_product()
    {

        $this->product->modifiers()->save($this->modifier);

        $this->assertEquals(1, $this->product->modifiers->count());
    }


}
