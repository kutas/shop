<?php

namespace Tests\Unit\Relations;

use App\CartItem;
use App\Order;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class OrderCartItemsTest extends TestCase
{
    use DatabaseTransactions;

    protected $order;

    protected $cartItems;


    protected function setUp()
    {
        parent::setUp();

        $this->order = factory(Order::class)->create();
        $this->cartItems = factory(CartItem::class, 2)->create(['order_id' => $this->order->id]);

    }

    public function test_order_has_cart_items()
    {
        $this->assertEquals(2, $this->order->cartItems()->count());
    }

}
