<?php

namespace Tests\Unit\Relations;

use App\Order;
use App\OrderStatus;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class OrderOrderStatusTest extends TestCase
{
    use DatabaseTransactions;

    protected $order;

    protected function setUp()
    {
        parent::setUp();

        $this->order = factory(Order::class)->create();
    }

    public function test_order_has_status()
    {
        $this->assertInstanceOf(OrderStatus::class, $this->order->status);
    }
}
