<?php

namespace Tests\Unit\Relations;

use App\Order;
use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class OrderPaymenTest extends TestCase
{
   protected $order;

   protected function setUp()
   {
       parent::setUp();

       $this->order = factory(Order::class)->create();
   }


    public function test_order_can_has_payments()
    {
        $this->assertEquals(1, $this->order->payment()->count());
    }
}
