<?php

namespace Tests\Unit\Relations;

use App\Order;
use App\Shipping;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class OrderShippingTest extends TestCase
{

    use DatabaseTransactions;

    protected $order;

    protected function setUp()
    {
        parent::setUp();

        $this->order = factory(Order::class)->create();
    }

    public function test_order_has_shipping()
    {
        $this->assertEquals(1, $this->order->shipping()->count());
    }

}
