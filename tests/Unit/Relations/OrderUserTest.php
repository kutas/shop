<?php

namespace Tests\Unit\Relations;

use App\Order;
use App\User;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class OrderUserTest extends TestCase
{
    use DatabaseTransactions;

    protected $user;
    protected $orders;

    protected function setUp()
    {
        parent::setUp();

        $this->user = factory(User::class)->create();
        $this->orders = factory(Order::class, 2)->create(['email' => $this->user->email]);

    }

    public function test_user_can_has_orders()
    {
        $this->assertEquals(2, $this->user->orders()->count());
    }

}
