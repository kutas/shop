<?php

namespace Tests\Unit\Relations;

use App\Product;
use App\Widget;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class ProductWidgetTest extends TestCase
{
    use DatabaseTransactions;

    protected $products;
    protected $widget;

    protected function setUp()
    {
        parent::setUp();

        $this->products = factory(Product::class, 2)->create();
        $this->widget= factory(Widget::class)->create();

        $this->widget->products()->sync($this->products->pluck('id'));

    }

    public function test_widget_has_many_products()
    {
        $this->assertCount(2, $this->widget->products);

    }

}
