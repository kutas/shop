<?php

namespace Tests\Unit;

use App\Shipping;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class ShippingTest extends TestCase
{
    use DatabaseTransactions;

    protected $data;

    protected function setUp()
    {
        parent::setUp();

        $this->data = factory(Shipping::class)->make()->toArray();
    }


    public function test_shipping_creation()
    {
        $shipping = Shipping::create($this->data);

        $this->assertInstanceOf(Shipping::class, $shipping);
    }
}
