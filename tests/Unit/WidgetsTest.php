<?php

namespace Tests\Unit;

use App\Widget;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class WidgetsTest extends TestCase
{

    use DatabaseTransactions;

    public function test_create_widget()
    {
        $widget = factory(Widget::class)->create();

        $this->assertInstanceOf(Widget::class, $widget);
    }
}
